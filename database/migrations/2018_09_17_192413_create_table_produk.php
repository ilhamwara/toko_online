<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProduk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produk', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_kategori');
            $table->integer('id_subkategori');
            $table->string('name');
            $table->text('description');
            $table->text('tag');
            $table->integer('diskon');
            $table->integer('harga');
            $table->integer('stock');
            $table->integer('featured');
            $table->integer('flash_sale');
            $table->integer('new_arrival');
            $table->integer('promo');
            $table->integer('aktif');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
