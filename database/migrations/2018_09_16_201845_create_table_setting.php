<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSetting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setting', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_toko');
            $table->string('email');
            $table->string('password_email');
            $table->string('phone');
            $table->text('alamat');
            $table->string('facebook');
            $table->string('twitter');
            $table->string('instagram');
            $table->string('copyright');
            $table->text('google_analytics');
            $table->text('google_adwords');
            $table->text('live_chat');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
