<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('slug', function(){
    echo str_slug('Lumin M2 Gaming Mouse Wireless Bluetooth Game');
});
Route::get('/', 'FrontController@index');
// Route::get('product', 'FrontController@produk');
Route::get('featured', 'FrontController@featured');
Route::get('promo', 'FrontController@promo');
Route::get('flash-sale', 'FrontController@flash_sale');
Route::get('new-arrival', 'FrontController@new_arrival');
Route::get('product/cat/{kat}', 'FrontController@kategori');
Route::get('product/{kat}/{sub}', 'FrontController@subkategori');
Route::get('product/{slug}', 'FrontController@produkdetail');
Route::get('login', 'FrontController@login');
Route::post('login', 'FrontController@postlogin');
Route::get('register', 'FrontController@register');
Route::post('register', 'FrontController@postregister');
Route::get('cart', 'FrontController@cart');
Route::get('cart/hapus/{id}', 'FrontController@hapus_cart');
Route::post('cart', 'FrontController@post_cart');
Route::get('checkout', 'FrontController@checkout');
Route::get('page/{page}', 'FrontController@page');
Route::get('administrator/login', 'AdminController@login');
Route::post('administrator/login', 'AdminController@postlogin');

Route::post('cek_kota', 'AjaxController@cekkota');
Route::post('cek_kecamatan', 'AjaxController@cekkecamatan');
Route::post('cek_courier', 'AjaxController@cekcourier');
Route::post('cek_sub', 'AjaxController@ceksub');

Route::group(['middleware' => ['session']], function () {

    Route::get('memberarea', 'MemberController@memberarea');
    Route::get('address_book', 'MemberController@address_book');
    Route::get('myorder', 'MemberController@myorder');
    Route::get('myorder/{id}', 'MemberController@detail_order');
    Route::get('billing', 'MemberController@billing');
    Route::get('tracking', 'MemberController@tracking');
    Route::post('tracking', 'MemberController@post_tracking');
    Route::post('update-profile', 'MemberController@profile');
    Route::post('checkout', 'MemberController@checkout');
    Route::post('checkout-payment/{kode}','MemberController@payment');
    Route::get('logout', 'FrontController@logout');

    //ROUTE ADMINISTRATOR
    Route::prefix('administrator')->group(function () {
        Route::get('dashboard', 'AdminController@dashboard');

        //CMS
        Route::get('page', 'PageController@index');
        Route::get('page/add', 'PageController@tambah');
        Route::get('page/edit/{id}', 'PageController@edit');
        Route::get('page/delete/{id}', 'PageController@delete');
        Route::post('page/add', 'PageController@post_tambah');
        Route::post('page/edit/{id}', 'PageController@post_edit');

        //MENU
        Route::get('menu', 'MenuController@index');
        Route::get('menu/add', 'MenuController@tambah');
        Route::get('menu/edit/{id}', 'MenuController@edit');
        Route::get('menu/delete/{id}', 'MenuController@delete');
        Route::post('menu/add', 'MenuController@post_tambah');
        Route::post('menu/edit/{id}', 'MenuController@post_edit');


        //SETTING SITE
        Route::get('setting_site', 'AdminController@setting');
        Route::post('setting_site', 'AdminController@post_setting');

        //BANNER
        Route::get('banner', 'BannerController@index');
        Route::get('banner/add', 'BannerController@tambah');
        Route::get('banner/edit/{id}', 'BannerController@edit');
        Route::get('banner/delete/{id}', 'BannerController@delete');
        Route::post('banner/add', 'BannerController@post_tambah');
        Route::post('banner/edit/{id}', 'BannerController@post_edit');

        //ARTIKEL
        Route::get('article', 'ArtikelController@index');
        Route::get('article/add', 'ArtikelController@tambah');
        Route::get('article/edit/{id}', 'ArtikelController@edit');
        Route::get('article/delete/{id}', 'ArtikelController@delete');
        Route::post('article/add', 'ArtikelController@post_tambah');
        Route::post('article/edit/{id}', 'ArtikelController@post_edit');

        //KATEGORI
        Route::get('categories/list', 'KategoriController@index');
        Route::get('categories/list/add', 'KategoriController@tambah');
        Route::get('categories/list/edit/{id}', 'KategoriController@edit');
        Route::get('categories/list/delete/{id}', 'KategoriController@delete');
        Route::post('categories/list/add', 'KategoriController@post_tambah');
        Route::post('categories/list/edit/{id}', 'KategoriController@post_edit');

        //KATEGORI
        Route::get('categories/sub', 'SubKategoriController@index');
        Route::get('categories/sub/add', 'SubKategoriController@tambah');
        Route::get('categories/sub/edit/{id}', 'SubKategoriController@edit');
        Route::get('categories/sub/delete/{id}', 'SubKategoriController@delete');
        Route::post('categories/sub/add', 'SubKategoriController@post_tambah');
        Route::post('categories/sub/edit/{id}', 'SubKategoriController@post_edit');

        //ORDER
        Route::get('order', 'OrderController@index');
        Route::get('order/detail/{id}', 'OrderController@detail');
        Route::get('order/pending', 'OrderController@pending');
        Route::get('order/confirm', 'OrderController@confirm');
        Route::get('order/confirm-shipping', 'OrderController@confirm_shipping');
        Route::get('order/rejected', 'OrderController@rejected');
        Route::get('order/approve/{id}','OrderController@approve');
        Route::get('order/tolak/{id}','OrderController@tolak');
        Route::get('order/shipping/{id}','OrderController@shipping');
        Route::post('order/resi/{id}','OrderController@resi');

        //PRODUK
        Route::get('products/list', 'ProdukController@index');
        Route::get('products/new_arrival', 'ProdukController@index');
        Route::get('products/promo', 'ProdukController@index');
        Route::get('products/featured', 'ProdukController@index');
        Route::get('products/flash_sale', 'ProdukController@index');
        
        Route::get('products/add', 'ProdukController@tambah');
        Route::get('products/edit/{id}', 'ProdukController@edit');
        Route::get('products/delete/{id}', 'ProdukController@delete');
        Route::post('products/add', 'ProdukController@post_tambah');
        Route::post('products/edit/{id}', 'ProdukController@post_edit');

    });

});

Route::get('page/{page}', 'FrontController@page');

// Auth::routes();
// Route::get('/home', 'HomeController@index')->name('home');
