@extends('layouts.master-admin')
@section('content')
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-certificate"></i>{{$title}}
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{url('administrator/dashboard')}}">Dashboard</a></li>
        <li><a href="{{url('administrator/page')}}">CMS</a></li>
        <li>{{$title}}</li>
    </ul>
    <!-- END Datatables Header -->

    <!-- Datatables Content -->
    <div class="block">
        <div class="block-title">
            <h2><strong>{{$title}}</strong></h2>
        </div>
        <div>
            <form action="{{url('administrator/page/edit/'.$page->id)}}" method="POST" class="form-horizontal form-bordered">
                {{csrf_field()}}
                <div class="form-group">
                    <label class="col-md-3 control-label">Menu</label>
                    <div class="col-md-9">
                        <select id="example-chosen" name="menu" class="select-chosen" data-placeholder="Choose a Menu.." required>
                            <option></option>
                            @foreach($menu as $data)
                                <option @if($data->id == $page->menu) selected @endif value="{{$data->id}}">{{$data->nama}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('menu'))
                          <span class="help-block">
                              <strong>{{ $errors->first('menu') }}</strong>
                          </span>
                      @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Description</label>
                    <div class="col-md-9">
                        <textarea id="textarea-ckeditor" name="description" class="ckeditor" required>{{$page->description}}</textarea>
                        @if ($errors->has('description'))
                          <span class="help-block">
                              <strong>{{ $errors->first('description') }}</strong>
                          </span>
                      @endif
                    </div>
                </div>
                <div class="form-group form-actions">
                    <div class="col-md-9 col-md-offset-3">
                        <a href="{{url('administrator/page')}}" class="btn btn-warning"><i class="fa fa-arrow-left"></i> Back</a>
                        <button class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- END Datatables Content -->
</div>
@endsection
@section('js')
<script src="{{asset('admin/js/helpers/ckeditor/ckeditor.js')}}"></script>
@endsection