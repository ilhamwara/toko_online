@extends('layouts.master-admin')
@section('content')
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-certificate"></i>{{$title}}
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{url('administrator/dashboard')}}">Dashboard</a></li>
        <li>{{$title}}</li>
    </ul>
    <!-- END Datatables Header -->

    <!-- Datatables Content -->
    <div class="block">
        <div class="block-title">
            <h2><strong>{{$title}}</strong></h2>
        </div>
        <div>
            <form action="{{url('administrator/setting_site')}}" method="POST" class="form-horizontal form-bordered">
                {{csrf_field()}}
                <div class="form-group">
                    <label class="col-md-3 control-label">Name Store</label>
                    <div class="col-md-9">
                        <input type="text" name="nama_toko" value="{{$set->nama_toko}}" class="form-control" placeholder="Enter Name.." required>
                        @if ($errors->has('nama_toko'))
                          <span class="help-block">
                              <strong>{{ $errors->first('nama_toko') }}</strong>
                          </span>
                      @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Email</label>
                    <div class="col-md-9">
                        <input type="email" name="email" value="{{$set->email}}" class="form-control" placeholder="Enter Email.." required>
                        @if ($errors->has('email'))
                          <span class="help-block">
                              <strong>{{ $errors->first('email') }}</strong>
                          </span>
                      @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Password Email</label>
                    <div class="col-md-9">
                        <input type="password" name="password_email" class="form-control" placeholder="Enter Password Email..">
                        @if ($errors->has('password_email'))
                          <span class="help-block">
                              <strong>{{ $errors->first('password_email') }}</strong>
                          </span>
                      @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Phone Number</label>
                    <div class="col-md-9">
                        <input type="text" name="phone" value="{{$set->phone}}" class="form-control" placeholder="Enter Phone Number.." required>
                        @if ($errors->has('phone'))
                          <span class="help-block">
                              <strong>{{ $errors->first('phone') }}</strong>
                          </span>
                      @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Address</label>
                    <div class="col-md-9">
                        <textarea name="alamat" class="form-control" required cols="30" placeholder="Enter Address.." rows="10">{{$set->alamat}}</textarea>
                        @if ($errors->has('alamat'))
                          <span class="help-block">
                              <strong>{{ $errors->first('alamat') }}</strong>
                          </span>
                      @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Facebook</label>
                    <div class="col-md-9">
                        <input type="text" name="facebook" value="{{$set->facebook}}" class="form-control" placeholder="Enter Facebook.." >
                        @if ($errors->has('facebook'))
                          <span class="help-block">
                              <strong>{{ $errors->first('facebook') }}</strong>
                          </span>
                      @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Twitter</label>
                    <div class="col-md-9">
                        <input type="text" name="twitter" value="{{$set->twitter}}" class="form-control" placeholder="Enter Twitter.." >
                        @if ($errors->has('twitter'))
                          <span class="help-block">
                              <strong>{{ $errors->first('twitter') }}</strong>
                          </span>
                      @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Instagram</label>
                    <div class="col-md-9">
                        <input type="text" name="instagram" value="{{$set->instagram}}" class="form-control" placeholder="Enter Instagram.." >
                        @if ($errors->has('instagram'))
                          <span class="help-block">
                              <strong>{{ $errors->first('instagram') }}</strong>
                          </span>
                      @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Copyright</label>
                    <div class="col-md-9">
                        <input type="text" name="copyright" value="{{$set->copyright}}" class="form-control" placeholder="Enter Copyright.." >
                        @if ($errors->has('copyright'))
                          <span class="help-block">
                              <strong>{{ $errors->first('copyright') }}</strong>
                          </span>
                      @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Script Google Analytics</label>
                    <div class="col-md-9">
                        <textarea name="google_analytics" class="form-control" cols="30" placeholder="Enter Script Google Analytics.." rows="10">{{$set->google_analytics}}</textarea>
                        @if ($errors->has('google_analytics'))
                          <span class="help-block">
                              <strong>{{ $errors->first('google_analytics') }}</strong>
                          </span>
                      @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Script Google Adwords</label>
                    <div class="col-md-9">
                        <textarea name="google_adwords" class="form-control" cols="30" placeholder="Enter Script Google Adwords.." rows="10">{{$set->google_adwords}}</textarea>
                        @if ($errors->has('google_adwords'))
                          <span class="help-block">
                              <strong>{{ $errors->first('google_adwords') }}</strong>
                          </span>
                      @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Plugin Live Chat</label>
                    <div class="col-md-9">
                        <textarea name="live_chat" class="form-control" cols="30" placeholder="Enter Plugin Live Chat.." rows="10">{{$set->live_chat}}</textarea>
                        @if ($errors->has('live_chat'))
                          <span class="help-block">
                              <strong>{{ $errors->first('live_chat') }}</strong>
                          </span>
                      @endif
                    </div>
                </div>
                <div class="form-group form-actions">
                    <div class="col-md-9 col-md-offset-3">
                        <button class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- END Datatables Content -->
</div>
@endsection
@section('js')
@endsection