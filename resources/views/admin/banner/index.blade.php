@extends('layouts.master-admin')
@section('content')
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-certificate"></i>{{$title}}
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{url('administrator/dashboard')}}">Dashboard</a></li>
        <li>{{$title}}</li>
    </ul>
    <!-- END Datatables Header -->

    <!-- Datatables Content -->
    <div class="block">
        <div class="block-title clearfix">
            <h2><strong>List Data</strong></h2>
            <div class="block-options pull-right">
                <div class="btn-group btn-group">
                    <a href="{{url('administrator/banner/add')}}" class="btn btn-sm btn-warning">Add {{$title}}</a>
                </div>
            </div>
        </div>
        <div class="block-content-full">
            <div class="table-responsive">
                <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">No</th>
                            <th class="text-center">Kategori</th>
                            <th class="text-center">Type</th>
                            <th class="text-center">Link</th>
                            <th class="text-center">Image</th>
                            <th class="text-center">Created_at</th>
                            <th class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($banner as $k => $data)
                        <tr>
                            <td class="text-center">{{$k+1}}</td>
                            <td class="text-center">{{$data->id_kategori}}</td>
                            <td class="text-center">{{$data->type}}</td>
                            <td class="text-center">{{$data->link}}</td>
                            <td class="text-center">
                                <img src="{{asset('images/slider/'.$data->img)}}" class="img-responsive" width="100" style="margin: auto;">
                            </td>
                            <td class="text-center"><span class="label label-info">{{$data->created_at}}</span></td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <a href="{{url('administrator/banner/edit/'.$data->id)}}" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
                                    <a href="{{url('administrator/banner/delete/'.$data->id)}}" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- END Datatables Content -->
</div>
@endsection
@section('js')
<script src="{{asset('admin/js/pages/tablesDatatables.js')}}"></script>
<script>$(function(){ TablesDatatables.init(); });</script>
@endsection