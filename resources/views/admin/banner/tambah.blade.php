@extends('layouts.master-admin')
@section('content')
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-certificate"></i>{{$title}}
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{url('administrator/dashboard')}}">Dashboard</a></li>
        <li><a href="{{url('administrator/banner')}}">Banner</a></li>
        <li>{{$title}}</li>
    </ul>
    <!-- END Datatables Header -->

    <!-- Datatables Content -->
    <div class="block">
        <div class="block-title">
            <h2><strong>{{$title}}</strong></h2>
        </div>
        <div>
            <form action="{{url('administrator/banner/add')}}" method="POST" class="form-horizontal form-bordered" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="form-group">
                    <label class="col-md-3 control-label">Type</label>
                    <div class="col-md-9">
                        <select name="type" class="select-chosen form-control type" data-placeholder="Choose a Type.." required>
                            <option value="">-- Pilih Type Banner --</option>
                            <option value="Slider">Slider</option>
                            <option value="Banner Category">Banner Category</option>
                            {{-- <option value="Extra Home Banner">Extra Home Banner</option> --}}
                            {{-- <option value="Detail Product">Detail Product</option> --}}
                        </select>
                        @if ($errors->has('type'))
                          <span class="help-block">
                              <strong>{{ $errors->first('type') }}</strong>
                          </span>
                      @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Kategori</label>
                    <div class="col-md-9">
                        <select name="id_kategori" class="select-chosen form-control kat" disabled>
                            <option value="">-- Pilih Kategori --</option>
                            @foreach($kat as $dat_kat)
                            <option value="{{$dat_kat->id}}">{{$dat_kat->name}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('id_kategori'))
                          <span class="help-block">
                              <strong>{{ $errors->first('id_kategori') }}</strong>
                          </span>
                      @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Link</label>
                    <div class="col-md-9">
                        <input type="text" name="link" class="form-control" placeholder="Enter Link..">
                        @if ($errors->has('link'))
                          <span class="help-block">
                              <strong>{{ $errors->first('link') }}</strong>
                          </span>
                      @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Image</label>
                    <div class="col-md-9">
                        <input type="file" name="img" class="form-control" required>
                        @if ($errors->has('img'))
                          <span class="help-block">
                              <strong>{{ $errors->first('img') }}</strong>
                          </span>
                      @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Order Number</label>
                    <div class="col-md-9">
                        <input type="number" name="urutan" class="form-control" placeholder="Enter Order Number.." required>
                        @if ($errors->has('urutan'))
                          <span class="help-block">
                              <strong>{{ $errors->first('urutan') }}</strong>
                          </span>
                      @endif
                    </div>
                </div>
                <div class="form-group form-actions">
                    <div class="col-md-9 col-md-offset-3">
                        <a href="{{url('administrator/banner')}}" class="btn btn-warning"><i class="fa fa-arrow-left"></i> back</a>
                        <button class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- END Datatables Content -->
</div>
@endsection
@section('js')
<script>
  $('.type').change(function(){
    if (($(this).find(':selected').val() == 'Banner Category') || ($(this).find(':selected').val() == 'Slider')) {
      $('.kat').removeAttr('disabled');
    }else{
      $('.kat').attr('disabled',true);
    }
  })
</script>
@endsection