<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Toko Online - Login Administrator</title>
        <meta name="description" content="Login Administrator Toko Online">
        <meta name="author" content="indowebremote">
        <meta name="robots" content="index,follow">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <link rel="stylesheet" href="{{asset('admin/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('admin/css/plugins.css')}}">
        <link rel="stylesheet" href="{{asset('admin/css/main.css')}}">
        <link rel="stylesheet" href="{{asset('admin/css/themes.css')}}">
        <script src="{{asset('admin/js/vendor/modernizr.min.js')}}"></script>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- Login Container -->
                    <div id="login-container">
                        <!-- Login Title -->
                        <div class="login-title text-center">
                            <h1><strong>Login Admintrator Panel</strong></h1>
                        </div>
                        <!-- END Login Title -->

                        <!-- Login Block -->
                        <div class="block push-bit">
                            <!-- Login Form -->
                            <form action="{{url('administrator/login')}}" method="POST"  class="form-horizontal">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
                                            <input type="text" name="email" class="form-control input-lg" placeholder="Email" required>
                                        </div>
                                        @if ($errors->has('email'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('email') }}</strong>
                                          </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
                                            <input type="password" name="password" class="form-control input-lg" placeholder="Password" required>
                                        </div>
                                        @if ($errors->has('password'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('password') }}</strong>
                                          </span>
                                        @endif                                        
                                    </div>
                                </div>
                                <div class="form-group form-actions">
                                    <div class="col-xs-12 text-right">
                                        <button class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Login to Dashboard</button>
                                    </div>
                                </div>
                            </form>
                            <!-- END Login Form -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Login Alternative Row -->

        <script src="{{asset('admin/js/vendor/jquery.min.js')}}"></script>
        <script src="{{asset('admin/js/vendor/bootstrap.min.js')}}"></script>
        <script src="{{asset('admin/js/plugins.js')}}"></script>
        <script src="{{asset('admin/js/app.js')}}"></script>
        <script src="{{asset('admin/js/pages/login.js')}}"></script>
        @include('include.alert')
    </body>
</html>