@extends('layouts.master-admin')
@section('content')
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-certificate"></i>{{$title}}
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{url('administrator/dashboard')}}">Dashboard</a></li>
        <li><a href="{{url('administrator/menu')}}">Menu</a></li>
        <li>{{$title}}</li>
    </ul>
    <!-- END Datatables Header -->

    <!-- Datatables Content -->
    <div class="block">
        <div class="block-title">
            <h2><strong>{{$title}}</strong></h2>
        </div>
        <div>
            <form action="{{url('administrator/menu/edit/'.$menu->id)}}" method="POST" class="form-horizontal form-bordered">
                {{csrf_field()}}
                <div class="form-group">
                    <label class="col-md-3 control-label">Title</label>
                    <div class="col-md-9">
                        <select name="title" class="select-chosen" data-placeholder="Choose a Title.." required>
                            <option></option>
                            <option @if($menu->title == 'Information') selected @endif value="Information">Information</option>
                            <option @if($menu->title == 'Insider') selected @endif value="Insider">Insider</option>
                            <option @if($menu->title == 'Service') selected @endif value="Service">Service</option>
                        </select>
                        @if ($errors->has('title'))
                          <span class="help-block">
                              <strong>{{ $errors->first('title') }}</strong>
                          </span>
                      @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Name</label>
                    <div class="col-md-9">
                        <input type="text" name="nama" value="{{$menu->nama}}" class="form-control" placeholder="Enter Name.." required>
                        @if ($errors->has('nama'))
                          <span class="help-block">
                              <strong>{{ $errors->first('nama') }}</strong>
                          </span>
                      @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Link</label>
                    <div class="col-md-9">
                        <input type="text" name="link" value="{{$menu->link}}" class="form-control" placeholder="Enter Link.." required>
                        @if ($errors->has('link'))
                          <span class="help-block">
                              <strong>{{ $errors->first('link') }}</strong>
                          </span>
                      @endif
                    </div>
                </div>
                <div class="form-group form-actions">
                    <div class="col-md-9 col-md-offset-3">
                        <a href="{{url('administrator/menu')}}" class="btn btn-warning"><i class="fa fa-arrow-left"></i> back</a>
                        <button class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- END Datatables Content -->
</div>
@endsection
@section('js')
@endsection