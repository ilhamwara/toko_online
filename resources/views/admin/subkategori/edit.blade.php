@extends('layouts.master-admin')
@section('content')
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-certificate"></i>{{$title}}
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{url('administrator/dashboard')}}">Dashboard</a></li>
        <li><a href="{{url('administrator/categories/sub')}}">Sub Category</a></li>
        <li>{{$title}}</li>
    </ul>
    <!-- END Datatables Header -->

    <!-- Datatables Content -->
    <div class="block">
        <div class="block-title">
            <h2><strong>{{$title}}</strong></h2>
        </div>
        <div>
            <form action="{{url('administrator/categories/sub/edit/'.$sub->id)}}" method="POST" class="form-horizontal form-bordered">
                {{csrf_field()}}
                <div class="form-group">
                    <label class="col-md-3 control-label">Sub Kategori</label>
                    <div class="col-md-9">
                        <select name="id_kategori" class="select-chosen form-control" data-placeholder="Choose a Sub Kategori.." required>
                            <option value="">-- Pilih Sub Category --</option>
                            @foreach($kat as $data)
                            <option @if($sub->id_kategori == $data->id) selected @endif value="{{$data->id}}">{{$data->name}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('id_kategori'))
                          <span class="help-block">
                              <strong>{{ $errors->first('id_kategori') }}</strong>
                          </span>
                      @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Name</label>
                    <div class="col-md-9">
                        <input type="text" name="name" value="{{$sub->name}}" class="form-control" placeholder="Enter Name.." required>
                        @if ($errors->has('name'))
                          <span class="help-block">
                              <strong>{{ $errors->first('name') }}</strong>
                          </span>
                      @endif
                    </div>
                </div>
                <div class="form-group form-actions">
                    <div class="col-md-9 col-md-offset-3">
                        <a href="{{url('administrator/categories/sub')}}" class="btn btn-warning"><i class="fa fa-arrow-left"></i> back</a>
                        <button class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- END Datatables Content -->
</div>
@endsection
@section('js')
@endsection