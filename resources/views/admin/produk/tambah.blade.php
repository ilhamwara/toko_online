@extends('layouts.master-admin')
@section('content')
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-certificate"></i>{{$title}}
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{url('administrator/dashboard')}}">Dashboard</a></li>
        <li><a href="{{url('administrator/products/list')}}">Products</a></li>
        <li>{{$title}}</li>
    </ul>
    <!-- END Datatables Header -->

    <!-- Datatables Content -->
    <div class="block">
        <div class="block-title">
            <h2><strong>{{$title}}</strong></h2>
        </div>
        <div>
            <form action="{{url('administrator/products/add')}}" method="POST" class="form-horizontal form-bordered" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="form-group">
                    <label class="col-md-3 control-label">Name</label>
                    <div class="col-md-9">
                        <input type="text" name="name" class="form-control" placeholder="Enter Name.." required>
                        @if ($errors->has('name'))
                          <span class="help-block">
                              <strong>{{ $errors->first('name') }}</strong>
                          </span>
                      @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Type Product</label>
                    <div class="col-md-9">
                        <select name="type" class="select-chosen form-control" data-placeholder="Choose a Type Product..">
                            <option>-- Type Product --</option>
                            <option value="Featured">Featured</option>
                            <option value="Flash Sale">Flash Sale</option>
                            <option value="New Arrival">New Arrival</option>
                            <option value="Promo">Promo</option>
                        </select>
                        @if ($errors->has('type'))
                          <span class="help-block">
                              <strong>{{ $errors->first('type') }}</strong>
                          </span>
                      @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Category</label>
                    <div class="col-md-9">
                        <select name="id_kategori" class="select-chosen kategori form-control" data-placeholder="Choose a Category.." required>
                            <option>-- Category --</option>
                            @foreach($kat as $dat_kat)
                            <option value="{{$dat_kat->id}}">{{$dat_kat->name}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('id_kategori'))
                          <span class="help-block">
                              <strong>{{ $errors->first('id_kategori') }}</strong>
                          </span>
                      @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Sub Category</label>
                    <div class="col-md-9">
                        <select name="id_subkategori" class="select-chosen subkategori form-control" data-placeholder="Choose a Sub Category.." required disabled>
                            <option>-- Sub Category --</option>
                        </select>
                        @if ($errors->has('id_subkategori'))
                          <span class="help-block">
                              <strong>{{ $errors->first('id_subkategori') }}</strong>
                          </span>
                      @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Description</label>
                    <div class="col-md-9">
                        <textarea id="textarea-ckeditor" name="description" class="ckeditor" required></textarea>
                        @if ($errors->has('description'))
                          <span class="help-block">
                              <strong>{{ $errors->first('description') }}</strong>
                          </span>
                      @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Tags</label>
                    <div class="col-md-9">
                        <input type="text" name="tag" id="example-tags" class="input-tags form-control" placeholder="Enter Tags.." required>
                        @if ($errors->has('tag'))
                          <span class="help-block">
                              <strong>{{ $errors->first('tag') }}</strong>
                          </span>
                      @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Price</label>
                    <div class="col-md-9">
                        <input type="number" name="harga" class="form-control" placeholder="Enter Price.." required>
                        @if ($errors->has('harga'))
                          <span class="help-block">
                              <strong>{{ $errors->first('harga') }}</strong>
                          </span>
                      @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Discount</label>
                    <div class="col-md-9">
                        <input type="number" name="diskon" class="form-control" placeholder="Enter Discount.." required>
                        @if ($errors->has('diskon'))
                          <span class="help-block">
                              <strong>{{ $errors->first('diskon') }}</strong>
                          </span>
                      @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Stock</label>
                    <div class="col-md-9">
                        <input type="number" name="stock" class="form-control" placeholder="Enter Stock.." required>
                        @if ($errors->has('stock'))
                          <span class="help-block">
                              <strong>{{ $errors->first('stock') }}</strong>
                          </span>
                      @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Image</label>
                    <div class="col-md-7">
                        <input type="file" name="img[]" class="form-control" required>
                        @if ($errors->has('img'))
                          <span class="help-block">
                              <strong>{{ $errors->first('img') }}</strong>
                          </span>
                      @endif
                    </div>
                    <div class="col-md-1">
                        <a href="#" class="btn btn-primary">Add Image</a>
                    </div>
                </div>
                <div class="form-group form-actions">
                    <div class="col-md-9 col-md-offset-3">
                        <a href="{{url('administrator/products')}}" class="btn btn-warning"><i class="fa fa-arrow-left"></i> back</a>
                        <button class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- END Datatables Content -->
</div>
@endsection
@section('js')
<script src="{{asset('admin/js/helpers/ckeditor/ckeditor.js')}}"></script>
<script>
  $('.kategori').change(function(){
    $('.subkategori').removeAttr('disabled');
    $.ajax({
        type : 'POST',
        url  : '{{url('cek_sub')}}',
        data : {
          id_kat  : $(this).find(':selected').val(),
          _token   : '{{csrf_token()}}'
        },
        beforeSend :function(){
          $('.sub').remove();
        },
        success: function(response){
          if($.isArray(response.data.post)){
            $.each(response.data.post, function(key, value){
              var pender = '<option class="sub" value="'+value.id+'">'+value.name+'</option>';
              $('.subkategori').append(pender);
            });
          }
        }
    });
  });
</script>
@endsection