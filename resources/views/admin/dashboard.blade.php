@extends('layouts.master-admin')
@section('content')
<div id="page-content">
    <div class="row text-center">
        <div class="col-sm-6 col-lg-4">
            <a href="javascript:void(0)" class="widget widget-hover-effect2">
                <div class="widget-extra themed-background">
                    <h4 class="widget-content-light"><strong>Pending</strong> Orders</h4>
                </div>
                <div class="widget-extra-full"><span class="h2 animation-expandOpen">{{$order_pending}}</span></div>
            </a>
        </div>
        <div class="col-sm-6 col-lg-4">
            <a href="javascript:void(0)" class="widget widget-hover-effect2">
                <div class="widget-extra themed-background-dark">
                    <h4 class="widget-content-light"><strong>Orders</strong> Today</h4>
                </div>
                <div class="widget-extra-full"><span class="h2 themed-color-dark animation-expandOpen">{{$order_today}}</span></div>
            </a>
        </div>
        <div class="col-sm-6 col-lg-4">
            <a href="javascript:void(0)" class="widget widget-hover-effect2">
                <div class="widget-extra themed-background-dark">
                    <h4 class="widget-content-light"><strong>Earnings</strong> Today</h4>
                </div>
                <div class="widget-extra-full"><span class="h2 themed-color-dark animation-expandOpen">Rp {{str_replace(',','.',number_format($earning))}}</span></div>
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="block full">
                <div class="block-title">
                    <h2><strong>Order </strong> {{date('Y')}}</h2>
                </div>
                <canvas id="order" width="400" height="400"></canvas>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="block full">
                <div class="block-title">
                    <h2><strong>Earning</strong> {{date('Y')}}</h2>
                </div>
                <canvas id="myChart" width="400" height="400"></canvas>
            </div>
        </div>
    </div>   
    <div class="row">
        <div class="col-lg-6">
            <div class="block">
                <div class="block-title">
                    <h2><strong>Latest</strong> Orders</h2>
                </div>
                <table class="table table-borderless table-striped table-vcenter table-bordered">
                    <tbody>
                        @foreach($trans as $dat_trans)
                        <tr>
                            <td class="text-center"><a href="javascript:void(0)"><strong>{{$dat_trans->kode_order}}</strong></a></td>
                            <td><a href="javascript:void(0)">{{$dat_trans->nama}}</a></td>
                            <td>{{date('d-m-Y',strtotime($dat_trans->created_at))}}</td>
                            <td>
                                @if($dat_trans->status == 0)
                                    <label class="label label-warning">Belum Melakukan Pembayaran</label>
                                @elseif($dat_trans->status == 1)
                                    <label class="label label-primary">Sudah Melakukan Pembayaran</label>
                                @elseif($dat_trans->status == 2)
                                    <label class="label label-success">Barang Dikirim Ke Courier</label>
                                @elseif($dat_trans->status == 3)
                                    <label class="label label-success">Sudah di Shipping</label>
                                @elseif($dat_trans->status == 4)
                                    <label class="label label-danger">Pembayaran Ditolak</label>
                                @endif
                            </td>
                            <td>
                                <a href="{{url('administrator/order/detail/'.$dat_trans->id)}}" class="label label-primary">Detail Order</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="block">
                <div class="block-title">
                    <h2><strong>Top</strong> Products</h2>
                </div>
                <table class="table table-borderless table-striped table-vcenter table-bordered">
                    <tbody>
                        @foreach($pro as $k => $dat_pro)
                        <tr>
                            <td class="text-center">{{$k+1}}</td>
                            <td>{{\App\Produk::where('id',$dat_pro->id_produk)->value('name')}}</td>
                            <td class="text-center"><strong>{{$dat_pro->count}}</strong> Orders</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>  
        </div>
    </div>    
</div>
@endsection
@section('js')
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0/dist/Chart.min.js"></script>
<script>
var ctx = document.getElementById('order').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun','Jul','Agust','Sept','Okt','Nov','Des'],
        datasets: [{
            label: 'Order',
            data: [
                {{\App\Transaksi::whereIn('transaksi.status',[2,3])->whereMonth('created_at',1)->count()}},
                {{\App\Transaksi::whereIn('transaksi.status',[2,3])->whereMonth('created_at',2)->count()}},
                {{\App\Transaksi::whereIn('transaksi.status',[2,3])->whereMonth('created_at',3)->count()}},
                {{\App\Transaksi::whereIn('transaksi.status',[2,3])->whereMonth('created_at',4)->count()}},
                {{\App\Transaksi::whereIn('transaksi.status',[2,3])->whereMonth('created_at',5)->count()}},
                {{\App\Transaksi::whereIn('transaksi.status',[2,3])->whereMonth('created_at',6)->count()}},
                {{\App\Transaksi::whereIn('transaksi.status',[2,3])->whereMonth('created_at',7)->count()}},
                {{\App\Transaksi::whereIn('transaksi.status',[2,3])->whereMonth('created_at',8)->count()}},
                {{\App\Transaksi::whereIn('transaksi.status',[2,3])->whereMonth('created_at',9)->count()}},
                {{\App\Transaksi::whereIn('transaksi.status',[2,3])->whereMonth('created_at',10)->count()}},
                {{\App\Transaksi::whereIn('transaksi.status',[2,3])->whereMonth('created_at',11)->count()}},
                {{\App\Transaksi::whereIn('transaksi.status',[2,3])->whereMonth('created_at',12)->count()}},
            ],
            backgroundColor: [
                'rgba(252, 92, 101,0.4)',
                'rgba(75, 123, 236,0.4)',
                'rgba(32, 191, 107,0.4)',
                'rgba(45, 152, 218,0.4)',
                'rgba(15, 185, 177,0.4)',
                'rgba(96, 163, 188,0.4)',
                'rgba(253, 121, 168,0.4)',
                'rgba(225, 112, 85,0.4)',
                'rgba(253, 203, 110,0.4)',
                'rgba(45, 52, 54,0.4)',
                'rgba(178, 190, 195,0.4)',
                'rgba(64, 64, 122,0.4)',
            ],
            borderColor: [
                'rgba(252, 92, 101,0.4)',
                'rgba(75, 123, 236,0.4)',
                'rgba(32, 191, 107,0.4)',
                'rgba(45, 152, 218,0.4)',
                'rgba(15, 185, 177,0.4)',
                'rgba(96, 163, 188,0.4)',
                'rgba(253, 121, 168,0.4)',
                'rgba(225, 112, 85,0.4)',
                'rgba(253, 203, 110,0.4)',
                'rgba(45, 52, 54,0.4)',
                'rgba(178, 190, 195,0.4)',
                'rgba(64, 64, 122,0.4)',
            ],
            borderWidth: 2
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});

var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun','Jul','Agust','Sept','Okt','Nov','Des'],
        datasets: [{
            label: 'Earning',
            data: [
                {{\App\TransaksiProduk::join('transaksi','transaksi_produk.id_transaksi','=','transaksi.id')->whereIn('transaksi.status',[2,3])->whereMonth('transaksi.created_at',1)->sum('total')}},
                {{\App\TransaksiProduk::join('transaksi','transaksi_produk.id_transaksi','=','transaksi.id')->whereIn('transaksi.status',[2,3])->whereMonth('transaksi.created_at',2)->sum('total')}},
                {{\App\TransaksiProduk::join('transaksi','transaksi_produk.id_transaksi','=','transaksi.id')->whereIn('transaksi.status',[2,3])->whereMonth('transaksi.created_at',3)->sum('total')}},
                {{\App\TransaksiProduk::join('transaksi','transaksi_produk.id_transaksi','=','transaksi.id')->whereIn('transaksi.status',[2,3])->whereMonth('transaksi.created_at',4)->sum('total')}},
                {{\App\TransaksiProduk::join('transaksi','transaksi_produk.id_transaksi','=','transaksi.id')->whereIn('transaksi.status',[2,3])->whereMonth('transaksi.created_at',5)->sum('total')}},
                {{\App\TransaksiProduk::join('transaksi','transaksi_produk.id_transaksi','=','transaksi.id')->whereIn('transaksi.status',[2,3])->whereMonth('transaksi.created_at',6)->sum('total')}},
                {{\App\TransaksiProduk::join('transaksi','transaksi_produk.id_transaksi','=','transaksi.id')->whereIn('transaksi.status',[2,3])->whereMonth('transaksi.created_at',7)->sum('total')}},
                {{\App\TransaksiProduk::join('transaksi','transaksi_produk.id_transaksi','=','transaksi.id')->whereIn('transaksi.status',[2,3])->whereMonth('transaksi.created_at',8)->sum('total')}},
                {{\App\TransaksiProduk::join('transaksi','transaksi_produk.id_transaksi','=','transaksi.id')->whereIn('transaksi.status',[2,3])->whereMonth('transaksi.created_at',9)->sum('total')}},
                {{\App\TransaksiProduk::join('transaksi','transaksi_produk.id_transaksi','=','transaksi.id')->whereIn('transaksi.status',[2,3])->whereMonth('transaksi.created_at',10)->sum('total')}},
                {{\App\TransaksiProduk::join('transaksi','transaksi_produk.id_transaksi','=','transaksi.id')->whereIn('transaksi.status',[2,3])->whereMonth('transaksi.created_at',11)->sum('total')}},
                {{\App\TransaksiProduk::join('transaksi','transaksi_produk.id_transaksi','=','transaksi.id')->whereIn('transaksi.status',[2,3])->whereMonth('transaksi.created_at',12)->sum('total')}},
            ],
            backgroundColor: [
                'rgba(252, 92, 101,0.4)',
                'rgba(75, 123, 236,0.4)',
                'rgba(32, 191, 107,0.4)',
                'rgba(45, 152, 218,0.4)',
                'rgba(15, 185, 177,0.4)',
                'rgba(96, 163, 188,0.4)',
                'rgba(253, 121, 168,0.4)',
                'rgba(225, 112, 85,0.4)',
                'rgba(253, 203, 110,0.4)',
                'rgba(45, 52, 54,0.4)',
                'rgba(178, 190, 195,0.4)',
                'rgba(64, 64, 122,0.4)',
            ],
            borderColor: [
                'rgba(252, 92, 101,0.4)',
                'rgba(75, 123, 236,0.4)',
                'rgba(32, 191, 107,0.4)',
                'rgba(45, 152, 218,0.4)',
                'rgba(15, 185, 177,0.4)',
                'rgba(96, 163, 188,0.4)',
                'rgba(253, 121, 168,0.4)',
                'rgba(225, 112, 85,0.4)',
                'rgba(253, 203, 110,0.4)',
                'rgba(45, 52, 54,0.4)',
                'rgba(178, 190, 195,0.4)',
                'rgba(64, 64, 122,0.4)',
            ],
            borderWidth: 2
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
</script>
@endsection