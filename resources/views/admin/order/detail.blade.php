@extends('layouts.master-admin')
@section('content')
<div id="page-content">
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-certificate"></i>{{$title}}
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{url('administrator/dashboard')}}">Dashboard</a></li>
        <li><a href="{{url('administrator/order')}}">List Order</a></li>
        <li>{{$title}}</li>
    </ul>
    <div class="block">
        <div class="block-title clearfix">
            <h2><strong>Order {{$trans->kode_order}}</strong></h2>
        </div>
        <div class="block-content-full" style="overflow: hidden;">
            <div class="table-responsive">
                <table class="table table-striped table-bordered">
                  <tr>
                    <th colspan="3" class="text-center" style="background:#34495e; color:#fff;">SHIPPING</th>
                  </tr>
                  <tr>
                    <td width="25%">Order Code</td>
                    <td class="text-center"  width="5%">:</td>
                    <td>{{$trans->kode_order}}</td>
                  </tr>
                  <tr>
                    <td width="25%">Status</td>
                    <td class="text-center"  width="5%">:</td>
                    <td>
                      @if($trans->status == 0)
                          <label for="" class="label label-warning">Order has not been paid</label>
                      @elseif($trans->status == 1)
                          <label for="" class="label label-primary">Order Has Been Paid</label>
                      @elseif($trans->status == 2)
                          <label for="" class="label label-success">Order Has Been Sent to Shippment</label>
                      @elseif($trans->status == 3)
                          <label for="" class="label label-success">Order Already Shippment</label>
                      @elseif($trans->status == 4)
                          <label for="" class="label label-danger">Payment Has Been Rejected</label>
                      @endif
                    </td>
                  </tr>
                  <tr>
                    <td width="25%">No Resi</td>
                    <td class="text-center"  width="5%">:</td>
                    <td>{{$trans->noresi}}</td>
                  </tr>
                  <tr>
                    <td width="25%">Name</td>
                    <td class="text-center"  width="5%">:</td>
                    <td>{{$trans->nama}}</td>
                  </tr>
                  <tr>
                    <td width="25%">Email</td>
                    <td class="text-center"  width="5%">:</td>
                    <td>{{$trans->email}}</td>
                  </tr>
                  <tr>
                    <td width="25%">Phone</td>
                    <td class="text-center"  width="5%">:</td>
                    <td>{{$trans->telp}}</td>
                  </tr>
                  <tr>
                    <td width="25%">Province</td>
                    <td class="text-center"  width="5%">:</td>
                    <td>{{$trans->provinsi}}</td>
                  </tr>
                  <tr>
                    <td>City</td>
                    <td class="text-center" >:</td>
                    <td>{{$trans->kota}}</td>
                  </tr>
                  <tr>
                    <td>Sub District</td>
                    <td class="text-center" >:</td>
                    <td>{{$trans->kecamatan}}</td>
                  </tr>
                  <tr>
                    <td>Full Address</td>
                    <td class="text-center" >:</td>
                    <td>{{$trans->alamat}}</td>
                  </tr>
                  <tr>
                    <td>Courier</td>
                    <td class="text-center" >:</td>
                    <td>{{$trans->kurir}}</td>
                  </tr>
                  <tr>
                    <td>Shipping Type</td>
                    <td class="text-center" >:</td>
                    <td>{{$trans->shipping_desc}}</td>
                  </tr>
                  <tr>
                    <td>Shipping Estimate</td>
                    <td class="text-center" >:</td>
                    <td>{{$trans->etd}}</td>
                  </tr>
                  <tr>
                    <td>Shipping Price</td>
                    <td class="text-center" >:</td>
                    <td>Rp {{str_replace(',','.',number_format($trans->shipping_price))}}</td>
                  </tr>
                </table>
                <table class="table table-striped table-bordered">
                  <tr>
                    <th colspan="4" class="text-center" style="background:#34495e; color:#fff;">PRODUCTS</th>
                  </tr>
                  <tr>
                    <td>Name</td>
                    <td>Image</td>
                    <td>QTY</td>
                    <td>Price</td>
                  </tr>
                  @foreach($pro as $data)
                  <tr>
                    <td>{{\App\Produk::where('id',$data->id_produk)->value('name')}}</td>
                    <td><img class="img-responsive" src="{{asset('images/products/'.\App\FotoProduk::where('id_produk',$data->id_produk)->value('img_thumbnail1'))}}" alt=""></td>
                    <td>{{$data->qty}}</td>
                    <td>Rp {{str_replace(',','.',number_format($data->total))}}</td>
                  </tr>
                  @endforeach
                  <tr>
                    <td colspan="3">Sub Total</td>
                    <td>Rp {{str_replace(',','.',number_format($pro->sum('total')))}}</td>
                  </tr>
                  <tr>
                    <td colspan="3">Shipping</td>
                    <td>Rp {{str_replace(',','.',number_format($trans->shipping_price))}}</td>
                  </tr>
                  <tr>
                    <td colspan="3">Grand Total</td>
                    <td>Rp {{str_replace(',','.',number_format($pro->sum('total')+$trans->shipping_price))}}</td>
                  </tr>
                </table>
                <table class="table table-striped table-bordered">
                  <tr>
                    <th colspan="4" class="text-center" style="background:#34495e; color:#fff;">BUKTI PEMBAYARAN</th>
                  </tr>
                  <tr>
                    <td width="20%">Nama Pemilik Akun Bank</td>
                    <td width="20%">Nama Akun Bank</td>
                    <td>Bukti Pembayaran</td>
                  </tr>
                  @foreach($bukti as $dat_bukti)
                  <tr>
                    <td>{{$dat_bukti->nama}}</td>
                    <td>{{$dat_bukti->bank}}</td>
                    <td>
                        <a href="{{asset('images/bukti/'.$dat_bukti->bukti)}}">
                            <img class="img-responsive" src="{{asset('images/bukti/'.$dat_bukti->bukti)}}" alt="">
                        </a>
                    </td>
                  </tr>
                  @endforeach
                </table>
            </div>
            <div class="col-md-12 text-center">
                @if($trans->status == 1)
                    <a href="{{url('administrator/order/approve/'.$trans->id)}}" class="btn btn-warning">Approve Pembayaran</a>
                    <a href="{{url('administrator/order/tolak/'.$trans->id)}}" class="btn btn-danger">Tolak Pembayaran</a>
                @elseif($trans->status == 2)
                    <a href="{{url('administrator/order/shipping/'.$trans->id)}}" class="btn btn-success">Update Status Kirim ke Courier</a>
                @elseif(($trans->status == 3))
                    <a data-toggle="modal" data-target="#shipping" class="btn btn-success">Update No Resi</a>
                @endif
                <br><br>
            </div>
        </div>
    </div>
</div>
@if($trans->status == 3)
   <div id="shipping" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Update Resi</h4>
          </div>
          <div class="modal-body">
            <form action="{{url('administrator/order/resi/'.$trans->id)}}" method="POST">
              {{csrf_field()}}
              <div class="form-group">
                <label>No Resi <sup style="color: red;">*</sup></label>
                <input type="text" class="form-control" name="resi" value="{{(empty($trans->noresi) ? '' : $trans->noresi)}}" placeholder="Masukan No Resi">
              </div>
              <div class="form-group">
                <button class="btn btn-success btn-block text-center"><span>Update</span></button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    @endif
@endsection
@section('js')
@endsection