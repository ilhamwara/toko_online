@extends('layouts.master-admin')
@section('content')
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-certificate"></i>{{$title}}
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{url('administrator/dashboard')}}">Dashboard</a></li>
        <li>{{$title}}</li>
    </ul>
    <!-- END Datatables Header -->

    <!-- Datatables Content -->
    <div class="block">
        <div class="block-title clearfix">
            <h2><strong>List Data</strong></h2>
        </div>
        <div class="block-content-full">
            <div class="table-responsive">
                <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">No</th>
                            <th class="text-center">Kode</th>
                            <th class="text-center">Tanggal</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($trans as $k => $data)
                        <tr>
                            <td class="text-center">{{$k+1}}</td>
                            <td class="text-center">{{$data->kode_order}}</td>
                            <td class="text-center">{{date('d-M-Y',strtotime($data->created_at))}}</td>
                            <td class="text-center">
                                @if($data->status == 0)
                                    <label class="label label-warning">Belum Melakukan Pembayaran</label>
                                @elseif($data->status == 1)
                                    <label class="label label-primary">Sudah Melakukan Pembayaran</label>
                                @elseif($data->status == 2)
                                    <label class="label label-success">Barang Dikirim Ke Courier</label>
                                @elseif($data->status == 3)
                                    <label class="label label-success">Sudah di Shipping</label>
                                @elseif($data->status == 4)
                                    <label class="label label-danger">Pembayaran Ditolak</label>
                                @endif
                            </td>
                            <td class="text-center">
                                @if($data->status == 1)
                                    <a href="{{url('administrator/order/approve/'.$data->id)}}" class="btn btn-block btn-warning">Approve Pembayaran</a>
                                    <a href="{{url('administrator/order/tolak/'.$data->id)}}" class="btn btn-block btn-danger">Tolak Pembayaran</a>
                                @elseif($data->status == 2)
                                    <a href="{{url('administrator/order/shipping/'.$data->id)}}" class="btn btn-block btn-success">Update Status Kirim ke Courier</a>
                                @endif
                                <a href="{{url('administrator/order/detail/'.$data->id)}}" class="btn btn-block btn-info">Detail</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- END Datatables Content -->
</div>
@endsection
@section('js')
<script src="{{asset('admin/js/pages/tablesDatatables.js')}}"></script>
<script>$(function(){ TablesDatatables.init(); });</script>
@endsection