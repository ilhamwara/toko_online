@extends('layouts.master')
@section('content')
<div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a title="Go to Home Page" href="{{url('/')}}">Home</a><span>&raquo;</span></li>
            <li><strong>Signup</strong></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- Main Container -->
  <section class="main-container col1-layout">
    <div class="main container">
      <div class="page-content">
        <div class="account-login">
          <form action="{{url('register')}}" method="POST">
                {{csrf_field()}}
          <div class="box-authentication new-customer-box Account Page ">
                <div class="row">
                  <div class="col-xs-12">
                    <div class="check-title">
                      <h4>SignUp</h4>
                    </div>
                  </div>
                  <div class="col-xs-12">
                    <label>Full Name:</label>
                    <div class="form-group">
                      <input type="text" name="name" class="form-control" required autofocus>
                      @if ($errors->has('name'))
                          <span class="help-block">
                              <strong>{{ $errors->first('name') }}</strong>
                          </span>
                      @endif
                    </div>
                  </div>
                  <div class="col-xs-12">
                    <label>Address:</label>
                    <div class="form-group">
                      <input type="text" name="alamat" class="form-control" required>
                      @if ($errors->has('alamat'))
                          <span class="help-block">
                              <strong>{{ $errors->first('alamat') }}</strong>
                          </span>
                      @endif
                    </div>
                  </div>
                  <div class="col-xs-12">
                    <label>City/Town:</label>
                    <div class="form-group">
                      <input type="text" name="kota" class="form-control" required>
                      @if ($errors->has('kota'))
                          <span class="help-block">
                              <strong>{{ $errors->first('kota') }}</strong>
                          </span>
                      @endif
                    </div>
                  </div>
                  <div class="col-xs-12">
                    <label>Email:</label>
                    <div class="form-group">
                      <input type="text" name="email" class="form-control" required>
                      @if($errors->has('email'))
                          <span class="help-block">
                              <strong>{{ $errors->first('email') }}</strong>
                          </span>
                      @endif
                    </div>
                  </div>
                  <div class="col-xs-12">
                    <label>Phone:</label>
                    <div class="form-group">
                      <input type="text" name="phone" class="form-control" required>
                      @if ($errors->has('phone'))
                          <span class="help-block">
                              <strong>{{ $errors->first('phone') }}</strong>
                          </span>
                      @endif
                    </div>
                  </div>
                  <div class="col-xs-12">
                    <label>Password:</label>
                    <div class="form-group">
                      <input type="password" name="password" class="form-control" required>
                      @if ($errors->has('password'))
                          <span class="help-block">
                              <strong>{{ $errors->first('password') }}</strong>
                          </span>
                      @endif
                    </div>
                  </div>
                  <div class="col-xs-12">
                    <div class="submit-text">
                      <p class="forgot-pass"><a href="{{url('login')}}">Login ?</a></p>
                      <button class="button"><i class="fa fa-user"></i>&nbsp; <span>Register</span></button>
                    </div>
                  </div>
                </div>
          </div>
          </form>
        </div>
      </div>
    </div>
  </section>
  <!-- Main Container End -->
@endsection