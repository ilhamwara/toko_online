@extends('layouts.master')
@section('content')
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a title="Go to Home Page" href="{{url('/')}}">Home</a><span>&raquo;</span></li>
            <li><strong>Account Information</strong></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
<!-- Main Container -->
  <section class="main-container col2-left-layout">
    <div class="main container">
      <div class="row">
        <aside class="left sidebar col-sm-3 col-xs-12">
          <div class="sidebar-account block">
            <div class="sidebar-bar-title">
              <h3>My Account</h3>
            </div>
            <div class="block-content">
              @include('include.sidebar-member')
            </div>
          </div>
        </aside>
        <div class="col-main col-sm-9 col-xs-12">
          <div class="my-account">
            <div class="page-title">
              <h2>Account Information</h2>
            </div>
            <form action="{{url('update-profile')}}" method="POST">
              {{csrf_field()}}
              <div class="box-authentication" style="width: 100%;">
                <label>Fullname <span class="required">*</span></label>
                <input name="name" value="{{Auth::user()->name}}" type="text" class="form-control" required>
                @if ($errors->has('name'))
                  <span class="help-block">
                      <strong>{{ $errors->first('name') }}</strong>
                  </span>
                @endif
                <label> Your email <span class="required">*</span></label>
                <input name="email" value="{{Auth::user()->email}}" type="text" class="form-control" required>
                @if ($errors->has('email'))
                  <span class="help-block">
                      <strong>{{ $errors->first('email') }}</strong>
                  </span>
                @endif
                <label> Your address <span class="required">*</span></label>
                <input name="alamat" value="{{Auth::user()->alamat}}" type="text" class="form-control" required>
                @if ($errors->has('alamat'))
                  <span class="help-block">
                      <strong>{{ $errors->first('alamat') }}</strong>
                  </span>
                @endif
                <label> Your city <span class="required">*</span></label>
                <input name="kota" value="{{Auth::user()->kota}}" type="text" class="form-control" required>
                @if ($errors->has('kota'))
                  <span class="help-block">
                      <strong>{{ $errors->first('kota') }}</strong>
                  </span>
                @endif
                <label> Your phone <span class="required">*</span></label>
                <input name="phone" value="{{Auth::user()->phone}}" type="text" class="form-control" required>
                @if ($errors->has('password'))
                  <span class="help-block">
                      <strong>{{ $errors->first('password') }}</strong>
                  </span>
                @endif
                <label> New password</label>
                <input name="password" type="text" class="form-control">
                <button class="button"><i class="fa fa-save"></i>&nbsp; <span>Save</span></button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
   <!-- our clients Slider -->
@endsection