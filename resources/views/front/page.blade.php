@extends('layouts.master')
@section('content')
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a title="Go to Home Page" href="{{url('/')}}">Home</a><span>&raquo;</span></li>
            <li><strong>{{$page->title}}</strong></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="main container">
    <div class="row">
     <div class="about-page">
        <div class="col-xs-12 col-sm-12"> 
          {!!$page->description!!}
        </div>
      </div>
    </div>
  </div>
  <br>
@endsection