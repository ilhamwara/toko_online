@extends('layouts.master')
@section('content')
<div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a title="Go to Home Page" href="{{url('/')}}">Home</a><span>&raquo;</span></li>
            @if(!empty($subnya))
              <li class=""> <a href="{{url('product/cat/'.\App\Kategori::where('id',$subnya->id_kategori)->value('slug'))}}">{{\App\Kategori::where('id',$subnya->id_kategori)->value('name')}}</a><span>&raquo;</span></li>
            @endif
            <li><strong>{{$title}}</strong></li>
          </ul>
        </div>
      </div>
    </div>
</div>
<!-- Main Container -->
  <div class="main-container col2-left-layout">
    <div class="container">
    
      <div class="row">
        <div class="col-main col-sm-9 col-xs-12 col-sm-push-3">
          
          <div class="shop-inner">
            <div class="page-title">
              <h2>{{$title}}</h2>
            </div>
            <div class="category-description">
             <div class="slider-items-products">
                <div id="category-slider" class="product-flexslider hidden-buttons">
                    <div class="slider-items slider-width-col4">
                        @foreach($slider as $dat_slid)
                        <a href="#"><img src="{{asset('images/slider/'.$dat_slid->img)}}" alt="banner"></a>
                        @endforeach
                    </div>
                </div>
            </div>
            
            </div>
            <div class="toolbar">
              <div class="sorter">
                <div class="short-by">
                  <label>Sort By:</label>
                  <select>
                    <option selected="selected">Position</option>
                    <option>Name</option>
                    <option>Price</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="product-grid-area infinite-scroll">
              <ul class="products-grid hidden-md hidden-lg">
                @forelse($pro2 as $dat_pro2)
                <li class="item col-xs-6 col-sm-6">
                  <div class="product-item">
                    <div class="item-inner">
                      <div class="product-thumbnail">
                        <a href="{{url('product/'.$dat_pro2->slug)}}" class="product-item-photo"> <img class="product-image-photo" src="{{asset('images/products/'.\App\FotoProduk::where('id_produk',$dat_pro2->id)->value('img_full'))}}" alt="" style="width:100%;"></a> </div>
                      <div class="pro-box-info">
                        <div class="item-info">
                          <div class="info-inner">
                            <div class="item-title"> <h4><a title="{{title_case($dat_pro2->name)}}" href="{{url('product/'.$dat_pro2->slug)}}">{{title_case($dat_pro2->name)}}</a></h4> </div>
                            <div class="item-content">
                              <div class="item-price">
                                <div class="price-box">
                                  @if($dat_pro2->diskon > 0)
                                  <p class="special-price"> 
                                    <span class="price-label">Special Price</span> 
                                    <span class="price"> Rp {{str_replace(',','.',number_format($dat_pro2->harga-(($dat_pro2->harga/100)*$dat_pro2->diskon)))}} </span> 
                                  </p>
                                  <p class="old-price"> 
                                     <span class="price"> Rp {{str_replace(',','.',number_format($dat_pro2->harga))}} </span> 
                                  </p>
                                  @else
                                  <span class="regular-price"> 
                                    <span class="price">Rp {{str_replace(',','.',number_format($dat_pro2->harga))}}</span> 
                                  </span>
                                  @endif

                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="box-hover hidden-xs">
                          <div class="product-item-actions">
                            <div class="pro-actions">
                              <a href="{{url('product/'.$dat_pro2->slug)}}"  class="action add-to-cart button-cart" title="View Detail"> <span>View Detail</span> </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
                @empty
                <div class="item col-lg-12 col-md-12">
                  <div class="text-center">
                    <h4><b>EMPTY PRODUCTS</b></h4>
                    <br>
                  </div>
                </div>
                @endforelse
              </ul>
              <ul class="products-grid hidden-sm hidden-xs">
                @forelse($pro as $dat_pro)
                <li class="item col-lg-4 col-md-3">
                  <div class="product-item">
                    <div class="item-inner">
                      <div class="product-thumbnail">
                        <a href="{{url('product/'.$dat_pro->slug)}}" class="product-item-photo"> <img class="product-image-photo" src="{{asset('images/products/'.\App\FotoProduk::where('id_produk',$dat_pro->id)->value('img_full'))}}" alt="" style="width:100%;"></a> </div>
                      <div class="pro-box-info">
                        <div class="item-info">
                          <div class="info-inner">
                            <div class="item-title"> <h4><a title="{{title_case($dat_pro->name)}}" href="{{url('product/'.$dat_pro->slug)}}">{{title_case($dat_pro->name)}}</a></h4> </div>
                            <div class="item-content">
                              <div class="item-price">
                                <div class="price-box">
                                  @if($dat_pro->diskon > 0)
                                  <p class="special-price"> 
                                    <span class="price-label">Special Price</span> 
                                    <span class="price"> Rp {{str_replace(',','.',number_format($dat_pro->harga-(($dat_pro->harga/100)*$dat_pro->diskon)))}} </span> 
                                  </p>
                                  <p class="old-price"> 
                                     <span class="price"> Rp {{str_replace(',','.',number_format($dat_pro->harga))}} </span> 
                                  </p>
                                  @else
                                  <span class="regular-price"> 
                                    <span class="price">Rp {{str_replace(',','.',number_format($dat_pro->harga))}}</span> 
                                  </span>
                                  @endif

                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="box-hover hidden-xs">
                          <div class="product-item-actions">
                            <div class="pro-actions">
                              <a href="{{url('product/'.$dat_pro->slug)}}"  class="action add-to-cart button-cart" title="View Detail"> <span>View Detail</span> </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
                @empty
                <div class="item col-lg-12 col-md-12">
                  <div class="text-center">
                    <h4><b>EMPTY PRODUCTS</b></h4>
                    <br>
                  </div>
                </div>
                @endforelse
              </ul>
              <div class="pagination-area ">
                {{$pro->render()}}
              </div>
            </div>
          </div>
        </div>
        <aside class="sidebar col-sm-3 col-xs-12 col-sm-pull-9 hidden-xs">
          <div class="block shop-by-side">
            <div class="sidebar-bar-title">
              <h3>Categories</h3>
            </div>
            <div class="block-content">
              <div class="layered-Category">
                <div class="layered-content">
                  <ul class="check-box-list">
                    @foreach($kat as $a => $dat_kat)
                    <li>
                      <a href="{{url('product/cat/'.$dat_kat->slug)}}">{{$dat_kat->name}} <span class="count">({{\App\Produk::where('id_kategori',$dat_kat->id)->where('aktif',1)->count()}})</span></a> 
                      @if(\App\SubKategori::where('id_kategori',$dat_kat->id)->count() > 0)
                        <a class="pull-right" data-toggle="collapse" href="#collapse{{$a+1}}"><i class="fa fa-chevron-down"></i></a>
                        <div id="collapse{{$a+1}}" class="panel-collapse collapse">
                          <ul class="sub" style="margin-left: 10px;">
                            @foreach(\App\SubKategori::where('id_kategori',$dat_kat->id)->get() as $dat_sub)
                              <li>
                                <a href="{{url('product/'.$dat_kat->slug.'/'.$dat_sub->slug)}}">- {{$dat_sub->name}} <span class="count">({{\App\Produk::where('id_subkategori',$dat_sub->id)->where('aktif',1)->count()}})</span></a>
                              </li>
                            @endforeach
                          </ul>
                        </div>
                      @endif
                    </li>
                    @endforeach
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="single-img-add sidebar-add-slider ">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel"> 
              <ol class="carousel-indicators">
                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
              </ol>
              <div class="carousel-inner" role="listbox">
                <div class="item active"> 
                  <img src="http://laz-img-cdn.alicdn.com/images/ims-web/TB1tzBdUNYaK1RjSZFnXXa80pXa.png_240x240Q100.jpg_.webp" alt="slide1">
                  <div class="carousel-caption">
                    <h3><a href="single_product.html" title=" Sample Product">Sale Up to 50% off</a></h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    <a href="#" class="info">shopping Now</a> </div>
                </div>
                <div class="item"> 
                  <img src="{{asset('images/add-slide2.jpg')}}" alt="slide2">
                  <div class="carousel-caption">
                    <h3><a href="single_product.html" title=" Sample Product">Smartwatch Collection</a></h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    <a href="#" class="info">All Collection</a> </div>
                </div>
                <div class="item"> 
                  <img src="https://www.static-src.com/siva/asset//05_2019/Marvel2.jpg?output-format=webp" alt="slide3">
                  <div class="carousel-caption">
                    <h3><a href="single_product.html" title=" Sample Product">Summer Sale</a></h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                  </div>
                </div>
              </div>
              <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next"> <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> <span class="sr-only">Next</span> </a> </div>
          </div>
          {{-- <div class="block popular-tags-area ">
            <div class="sidebar-bar-title">
              <h3>Popular Tags</h3>
            </div>
            <div class="tag">
              <ul>
                <li><a href="#">Boys</a></li>
                <li><a href="#">Camera</a></li>
                <li><a href="#">good</a></li>
                <li><a href="#">Computers</a></li>
                <li><a href="#">Phone</a></li>
                <li><a href="#">clothes</a></li>
                <li><a href="#">girl</a></li>
                <li><a href="#">shoes</a></li>
                <li><a href="#">women</a></li>
                <li><a href="#">accessoties</a></li>
                <li><a href="#">View All Tags</a></li>
              </ul>
            </div>
          </div> --}}
        </aside>
      </div>
    </div>
  </div>
  <!-- Main Container End -->
@endsection
@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jscroll/2.4.1/jquery.jscroll.js"></script>
<script>
  $('ul.pagination').hide();
    $(function() {
        $('.infinite-scroll').jscroll({
            autoTrigger: true,
            loadingHtml: '<img class="center-block" src="https://thumbs.gfycat.com/LoathsomeVastBarnswallow-size_restricted.gif" alt="Loading..." width="100" />',
            padding: 0,
            nextSelector: '.pagination li.active + li a',
            contentSelector: 'div.infinite-scroll',
            callback: function() {
                $('ul.pagination').remove();
            }
        });
    });
</script>
@endsection