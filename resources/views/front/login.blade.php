@extends('layouts.master')
@section('content')
<div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a title="Go to Home Page" href="{{url('/')}}">Home</a><span>&raquo;</span></li>
            <li><strong>Login</strong></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- Main Container -->
  <section class="main-container col1-layout">
    <div class="main container">
      <div class="page-content">
        <div class="account-login">
          <form action="{{url('login')}}" method="POST">
            {{csrf_field()}}
            <div class="box-authentication">
              <h4>Log in</h4>
              <p class="before-login-text">Welcome back! Sign in to your account</p>
              <label for="emmail_login"> Your email <span class="required">*</span></label>
              <input name="email" type="text" class="form-control" required>
              @if ($errors->has('email'))
                  <span class="help-block">
                      <strong>{{ $errors->first('email') }}</strong>
                  </span>
              @endif
              <label for="password_login">Your password <span class="required">*</span></label>
              <input name="password" type="password" class="form-control" required>
              @if ($errors->has('password'))
                  <span class="help-block">
                      <strong>{{ $errors->first('password') }}</strong>
                  </span>
              @endif
              <p class="forgot-pass"><a href="{{url('register')}}">Register ?</a></p>
              <button class="button"><i class="fa fa-lock"></i>&nbsp; <span>Login</span></button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
  <!-- Main Container End --> 
@endsection