@extends('layouts.master')
@section('content')
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a title="Go to Home Page" href="{{url('/')}}">Home</a><span>&raquo;</span></li>
            <li><strong>Tracking</strong></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
<!-- Main Container -->
  <section class="main-container col2-left-layout">
    <div class="main container">
      <div class="row">
        <aside class="left sidebar col-sm-3 col-xs-12">
          <div class="sidebar-account block">
            <div class="sidebar-bar-title">
              <h3>My Account</h3>
            </div>
            <div class="block-content">
              @include('include.sidebar-member')
            </div>
          </div>
        </aside>
        <div class="col-main col-sm-9 col-xs-12">
          <div class="my-account">
            <div class="page-title">
              <h2>Tracking</h2>
            </div>
            <form action="{{url('tracking')}}" method="POST">
              {{csrf_field()}}
              <div class="box-authentication" style="width: 100%;">
                <label>Order Code <span class="required">*</span></label>
                <input name="order_code" type="text" class="form-control" placeholder="Please insert your order code here" required>
                @if ($errors->has('order_code'))
                  <span class="help-block">
                      <strong>{{ $errors->first('order_code') }}</strong>
                  </span>
                @endif
                <button class="button"><i class="fa fa-search"></i>&nbsp; <span>Search</span></button>
              </div>
            </form>
            @if(!empty($trans))
            <br><br>
            <table class="table table-striped table-bordered">
              <tr>
                <th colspan="3" class="text-center" style="background:#34495e; color:#fff;">SHIPPING</th>
              </tr>
              <tr>
                <td width="25%">Order Code</td>
                <td class="text-center"  width="5%">:</td>
                <td>{{$trans->kode_order}}</td>
              </tr>
              <tr>
                <td width="25%">Status</td>
                <td class="text-center"  width="5%">:</td>
                <td>
                  @if($trans->status == 0)
                      <label for="" class="label label-warning">Order has not been paid</label>
                  @elseif($trans->status == 1)
                      <label for="" class="label label-primary">Order Has Been Paid</label>
                  @elseif($trans->status == 2)
                      <label for="" class="label label-success">Order Has Been Sent to Shippment</label>
                  @elseif($trans->status == 3)
                      <label for="" class="label label-success">Order Already Shippment</label>
                  @elseif($trans->status == 4)
                      <label for="" class="label label-danger">Payment Has Been Rejected</label>
                  @endif
                </td>
              </tr>
              <tr>
                <td width="25%">No Resi</td>
                <td class="text-center"  width="5%">:</td>
                <td>{{$trans->noresi}}</td>
              </tr>
              <tr>
                <td width="25%">Name</td>
                <td class="text-center"  width="5%">:</td>
                <td>{{$trans->nama}}</td>
              </tr>
              <tr>
                <td width="25%">Email</td>
                <td class="text-center"  width="5%">:</td>
                <td>{{$trans->email}}</td>
              </tr>
              <tr>
                <td width="25%">Phone</td>
                <td class="text-center"  width="5%">:</td>
                <td>{{$trans->telp}}</td>
              </tr>
              <tr>
                <td width="25%">Province</td>
                <td class="text-center"  width="5%">:</td>
                <td>{{$trans->provinsi}}</td>
              </tr>
              <tr>
                <td>City</td>
                <td class="text-center" >:</td>
                <td>{{$trans->kota}}</td>
              </tr>
              <tr>
                <td>Sub District</td>
                <td class="text-center" >:</td>
                <td>{{$trans->kecamatan}}</td>
              </tr>
              <tr>
                <td>Full Address</td>
                <td class="text-center" >:</td>
                <td>{{$trans->alamat}}</td>
              </tr>
              <tr>
                <td>Courier</td>
                <td class="text-center" >:</td>
                <td>{{$trans->kurir}}</td>
              </tr>
              <tr>
                <td>Shipping Type</td>
                <td class="text-center" >:</td>
                <td>{{$trans->shipping_desc}}</td>
              </tr>
              <tr>
                <td>Shipping Estimate</td>
                <td class="text-center" >:</td>
                <td>{{$trans->etd}}</td>
              </tr>
              <tr>
                <td>Shipping Price</td>
                <td class="text-center" >:</td>
                <td>Rp {{str_replace(',','.',number_format($trans->shipping_price))}}</td>
              </tr>
            </table>
            <table class="table table-striped table-bordered">
              <tr>
                <th colspan="4" class="text-center" style="background:#34495e; color:#fff;">PRODUCTS</th>
              </tr>
              <tr>
                <td>Name</td>
                <td>Image</td>
                <td>QTY</td>
                <td>Price</td>
              </tr>
              @foreach($pro as $data)
              <tr>
                <td>{{\App\Produk::where('id',$data->id_produk)->value('name')}}</td>
                <td><img class="img-responsive" src="{{asset('images/products/'.\App\FotoProduk::where('id_produk',$data->id_produk)->value('img_thumbnail1'))}}" alt=""></td>
                <td>{{$data->qty}}</td>
                <td>Rp {{str_replace(',','.',number_format($data->total))}}</td>
              </tr>
              @endforeach
              <tr>
                <td colspan="3">Sub Total</td>
                <td>Rp {{str_replace(',','.',number_format($pro->sum('total')))}}</td>
              </tr>
              <tr>
                <td colspan="3">Shipping</td>
                <td>Rp {{str_replace(',','.',number_format($trans->shipping_price))}}</td>
              </tr>
              <tr>
                <td colspan="3">Grand Total</td>
                <td>Rp {{str_replace(',','.',number_format($pro->sum('total')+$trans->shipping_price))}}</td>
              </tr>
            </table>
            @endif
          </div>
        </div>
      </div>
    </div>
  </section>
   <!-- our clients Slider -->
@endsection