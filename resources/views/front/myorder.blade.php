@extends('layouts.master')
@section('content')
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a title="Go to Home Page" href="{{url('/')}}">Home</a><span>&raquo;</span></li>
            <li><strong>My Orders</strong></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
<!-- Main Container -->
  <section class="main-container col2-left-layout">
    <div class="main container">
      <div class="row">
        <aside class="left sidebar col-sm-3 col-xs-12">
          <div class="sidebar-account block">
            <div class="sidebar-bar-title">
              <h3>My Account</h3>
            </div>
            <div class="block-content">
              @include('include.sidebar-member')
            </div>
          </div>
        </aside>
        <div class="col-main col-sm-9 col-xs-12">
          <div class="my-account">
            <div class="page-title">
              <h2>My Orders</h2>
              <table class="table table-striped table-bordered">
                <tr>
                  <th>No</th>
                  <th>Kode</th>
                  <th>Tanggal Order</th>
                  <th>Status</th>
                  <th></th>
                </tr>
                @foreach($trans as $k => $data)
                  <tr>
                    <td>{{$k+1}}</td>
                    <td>{{$data->kode_order}}</td>
                    <td>{{date('d-M-Y',strtotime($data->created_at))}}</td>
                    <td>
                      @if($data->status == 0)
                          <label for="" class="label label-warning">Order has not been paid</label>
                      @elseif($data->status == 1)
                          <label for="" class="label label-primary">Order Has Been Paid</label>
                      @elseif($data->status == 2)
                          <label for="" class="label label-success">Order Has Been Sent to Shippment</label>
                      @elseif($data->status == 3)
                          <label for="" class="label label-success">Order Already Shippment</label>
                      @elseif($data->status == 4)
                          <label for="" class="label label-danger">Payment Has Been Rejected</label>
                      @endif
                    </td>
                    <td>
                      <a href="{{url('myorder/'.$data->id)}}" class="btn btn-block btn-success">Detail</a>
                    </td>
                  </tr>
                @endforeach
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
   <!-- our clients Slider -->
@endsection