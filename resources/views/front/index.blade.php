@extends('layouts.master')
@section('content')
<!-- Main Slider Area -->
  <div class="main-slider-area">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-xs-12"> 
          <!-- Main Slider -->
          <div class="main-slider">
            <div class="slider">
              <div id="mainSlider" class="nivoSlider slider-image"> 
                @foreach($slider as $dat_slid)
                  <a href="#">
                    <img src="{{asset('images/slider/'.$dat_slid->img)}}" title="#htmlcaption2"/> 
                  </a>
                @endforeach
              </div>
              {{-- 
              <div id="htmlcaption2" class="nivo-html-caption slider-caption-2">
                <div class="slider-progress"></div>
                <div class="slide-text slide-text-2">
                  <div class="middle-text">
                    <div class="cap-dec">
                      <h2 class="cap-dec wow rotateIn" data-wow-duration="1.1s" data-wow-delay="0s">SPRING 2016 </h2>
                      <h1 class="cap-dec wow flipInY" data-wow-duration="1.3s" data-wow-delay="0s"><span>20%</span> OFF ALL ITEMS</h1>
                      <p class="cap-dec wow lightSpeedIn hidden-xs" data-wow-duration="1.5s" data-wow-delay="0s"> Loren ipsum dolorsit amet, consectetur adipisicing, sed do eiusmod.</p>
                    </div>
                    <div class="cap-readmore wow zoomInUp" data-wow-duration="1.3s" data-wow-delay=".3s"> <a href="#">Shop Now</a> </div>
                  </div>
                </div>
              </div> --}}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End Main Slider Area --> 
  <!-- Banner block-->
  <div class="container">
    <div class="row">
      <div class="jtv-banner-block">
        <div class="jtv-subbanner1 col-sm-4"><a href="{{url('featured')}}"><img class="img-respo" alt="jtv-subbanner1" src="https://www.static-src.com/siva/asset//05_2019/Gopay-terbaru.jpg?output-format=webp"></a>
          <div class="text-block">
            {{-- <div class="text1 wow animated fadeInUpBig animated"><a href="#">Featured</a></div> --}}
            {{-- <div class="text3 wow animated flip animated"><a href="{{url('featured')}}">Shop Now</a></div> --}}
          </div>
        </div>
        <div class="jtv-subbanner2 col-sm-4"><a href="{{url('promo')}}"><img class="img-respo" alt="jtv-subbanner2" src="https://www.static-src.com/siva/asset//05_2019/gesits-motor-listrik-360x240.jpg?output-format=webp"></a>
          <div class="text-block">
            {{-- <div class="text1 wow animated fadeInUpBig animated"><a href="#">Promo</a></div> --}}
            {{-- <div class="text3 wow animated flip animated"><a href="{{url('promo')}}">Shop Now</a></div> --}}
          </div>
        </div>
        <div class="jtv-subbanner2 col-sm-4"><a href="{{url('new-arrival')}}"><img class="img-respo" alt="jtv-subbanner2" src="https://www.static-src.com/siva/asset//05_2019/360x240_Promo_Terbaru_VISA-Promo.jpg?output-format=webp"></a>
          <div class="text-block">
            {{-- <div class="text1 wow animated fadeInUpBig animated"><a href="#">New Arrivals</a></div> --}}
            {{-- <div class="text3 wow animated flip animated"><a href="{{url('new-arrival')}}">Shop Now</a></div> --}}
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <!-- main container -->
  <div class="home-tab">
    <div class="container">
      <div class="row"> 
        {{-- <div class="jtv-promotion">
          <div class="container">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="wrap">
                  <div class="wow animated fadeInRight animated">
                    <div class="box jtv-custom">
                      <div class="box-content">
                        <div class="promotion-center">
                          <p class="text_medium">Limited Time Only</p>
                            <a href="{{url('flash-sale')}}">
                              <div class="text_large">
                                <div class="theme-color">45% off</div> 
                                Flash Sale
                              </div>
                            </a>
                            <p class="text_small">Fashion for all outerwear, shirt &amp; accessories</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div> --}}
        <div class="jtv-banner-block">
          <div class="jtv-subbanner1 col-md-6">
            <a href="{{url('flash-sale')}}">
              <img class="img-respo"  src="https://www.static-src.com/siva/asset//04_2019/bango-bumbu-blm-homepage-offstore.jpg?output-format=webp">
            </a>
          </div>
          <div class="jtv-subbanner1 col-md-6">
            <a href="{{url('flash-sale')}}">
              <img class="img-respo"  src="https://www.static-src.com/siva/asset//05_2019/573x191-flashsale-redmi7-may.jpg?output-format=webp">
            </a>
          </div>
        </div>
        <div class="clearfix"></div>
        <br>
        <!-- Best selling -->
        <div class="container">
          <div class="row">
            <div class="best-selling-slider col-sm-12">
              <div class="title_block">
                <h3 class="products_title">Best seller</h3>
              </div>
              <div class="slider-items-products">
                <div id="best-selling-slider" class="product-flexslider hidden-buttons">
                  <div class="slider-items slider-width-col4">
                    @php 
                    $trans  = \App\TransaksiProduk::join('produk','transaksi_produk.id_produk','=','produk.id')->where('transaksi_produk.status',1)->where('aktif',1)->inRandomOrder()->limit(12)->get();
                    $pro    = \App\Produk::where('aktif',1)->where('featured',0)->where('flash_sale',0)->where('new_arrival',0)->where('promo',0)->inRandomOrder()->limit(12)->get();
                    $data   = (count($trans) > 0 ? $trans : $pro);
                    @endphp
                    @foreach($data as $dat_pro)
                      <div class="product-item">
                      <div class="item-inner">
                        <div class="product-thumbnail">
                          <div class="icon-sale-label sale-left">Sale</div>
                          <a href="{{url('product/'.$dat_pro->slug)}}" class="product-item-photo"> <img class="product-image-photo" src="{{asset('images/products/'.\App\FotoProduk::where('id_produk',$dat_pro->id)->value('img_full'))}}" alt=""></a> </div>
                          <div class="pro-box-info">
                            <div class="item-info">
                              <div class="info-inner">
                                <div class="item-title">
                                  <h4> <a title="{{title_case($dat_pro->name)}}" href="{{url('product/'.$dat_pro->slug)}}">{{title_case($dat_pro->name)}} </a></h4>
                                </div>
                                <div class="item-content">
                                  {{-- <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div> --}}
                                  <div class="item-price">
                                    <div class="price-box"> 
                                      @if($dat_pro->diskon > 0)
                                        <p class="special-price"> 
                                          <span class="price-label">Special Price</span> 
                                          <span class="price"> Rp {{str_replace(',','.',number_format($dat_pro->harga-(($dat_pro->harga/100)*$dat_pro->diskon)))}} </span> 
                                        </p>
                                        <p class="old-price"> 
                                          <span class="price-label">Regular Price:</span>
                                           <span class="price"> Rp {{str_replace(',','.',number_format($dat_pro->harga))}} </span> 
                                        </p>
                                        @else
                                        <span class="regular-price"> 
                                          <span class="price-label">Regular Price:</span>
                                          <span class="price">Rp {{str_replace(',','.',number_format($dat_pro->harga))}}</span> 
                                        </span>
                                        @endif
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="box-hover">
                              <div class="product-item-actions">
                                <div class="pro-actions">
                                  <a href="{{url('product/'.$dat_pro->slug)}}"  class="action add-to-cart button-cart" title="View Detail"> <span>View Detail</span> </a>
                                </div>
                              </div>
                            </div>
                          </div>
                      </div>
                      </div>
                    @endforeach
                    
                  </div>
                </div>
              </div>
            </div>
            
            <!-- Featured Products -->
            
            {{-- <div class="featured-products-slider col-sm-6">
              <div class="title_block">
                <h3 class="products_title">Featured products</h3>
              </div>
              <div class="slider-items-products">
                <div id="featured-products-slider" class="product-flexslider hidden-buttons">
                  <div class="slider-items slider-width-col4">
                    <div class="product-item">
                      <div class="item-inner">
                        <div class="product-thumbnail">
                          <a href="single_product.html" class="product-item-photo"> <img class="product-image-photo" src="{{asset('images/products/img16.jpg')}}" alt=""></a> </div>
                        <div class="pro-box-info">
                          <div class="item-info">
                            <div class="info-inner">
                              <div class="item-title">
                                <h4> <a title="Product Title Here" href="single_product.html">Product Title Here </a></h4>
                              </div>
                              <div class="item-content">
                                <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                                <div class="item-price">
                                  <div class="price-box"> <span class="regular-price"> <span class="price">$125.00</span> </span> </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="box-hover">
                            <div class="product-item-actions">
                              <div class="pro-actions">
                                <button onclick="location.href='shopping_cart.html'" class="action add-to-cart" type="button" title="Add to Cart"> <span>Add to Cart</span> </button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="product-item">
                      <div class="item-inner">
                        <div class="product-thumbnail">
                          <a href="single_product.html" class="product-item-photo"> <img class="product-image-photo" src="{{asset('images/products/img18.jpg')}}" alt=""></a> </div>
                        <div class="pro-box-info">
                          <div class="item-info">
                            <div class="info-inner">
                              <div class="item-title">
                                <h4> <a title="Product Title Here" href="single_product.html">Product Title Here </a></h4>
                              </div>
                              <div class="item-content">
                                <div class="rating"> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                                <div class="item-price">
                                  <div class="price-box"> <span class="regular-price"> <span class="price">$175.88</span> </span> </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="box-hover">
                            <div class="product-item-actions">
                              <div class="pro-actions">
                                <button onclick="location.href='shopping_cart.html'" class="action add-to-cart" type="button" title="Add to Cart"> <span>Add to Cart</span> </button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="product-item">
                      <div class="item-inner">
                        <div class="product-thumbnail">
                          <div class="icon-new-label new-right">new</div>
                          <a href="single_product.html" class="product-item-photo"> <img class="product-image-photo" src="{{asset('images/products/img09.jpg')}}" alt=""></a> </div>
                        <div class="pro-box-info">
                          <div class="item-info">
                            <div class="info-inner">
                              <div class="item-title">
                                <h4> <a title="Product Title Here" href="single_product.html">Product Title Here </a></h4>
                              </div>
                              <div class="item-content">
                                <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> </div>
                                <div class="item-price">
                                  <div class="price-box">
                                    <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $299.00 </span> </p>
                                    <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> $399.00 </span> </p>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="box-hover">
                            <div class="product-item-actions">
                              <div class="pro-actions">
                                <button onclick="location.href='shopping_cart.html'" class="action add-to-cart" type="button" title="Add to Cart"> <span>Add to Cart</span> </button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="product-item">
                      <div class="item-inner">
                        <div class="product-thumbnail">
                          <a href="single_product.html" class="product-item-photo"> <img class="product-image-photo" src="{{asset('images/products/img11.jpg')}}" alt=""></a> </div>
                        <div class="pro-box-info">
                          <div class="item-info">
                            <div class="info-inner">
                              <div class="item-title">
                                <h4> <a title="Product Title Here" href="single_product.html">Product Title Here </a></h4>
                              </div>
                              <div class="item-content">
                                <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                                <div class="item-price">
                                  <div class="price-box"> <span class="regular-price"> <span class="price">$125.99</span> </span> </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="box-hover">
                            <div class="product-item-actions">
                              <div class="pro-actions">
                                <button onclick="location.href='shopping_cart.html'" class="action add-to-cart" type="button" title="Add to Cart"> <span>Add to Cart</span> </button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="product-item">
                      <div class="item-inner">
                        <div class="product-thumbnail">
                          <a href="single_product.html" class="product-item-photo"> <img class="product-image-photo" src="{{asset('images/products/img03.jpg')}}" alt=""></a> </div>
                        <div class="pro-box-info">
                          <div class="item-info">
                            <div class="info-inner">
                              <div class="item-title">
                                <h4> <a title="Product Title Here" href="single_product.html">Product Title Here </a></h4>
                              </div>
                              <div class="item-content">
                                <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                                <div class="item-price">
                                  <div class="price-box">
                                    <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $188.80 </span> </p>
                                    <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> $299.00 </span> </p>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="box-hover">
                            <div class="product-item-actions">
                              <div class="pro-actions">
                                <button onclick="location.href='shopping_cart.html'" class="action add-to-cart" type="button" title="Add to Cart"> <span>Add to Cart</span> </button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div> --}}
          </div>
        </div>
        <!-- Offer banner -->
        {{-- <div class="container">
          <div class="row">
            <div class="offer-add">
              <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="jtv-banner-box banner-inner">
                  <div class="image"> <a class="jtv-banner-opacity" href="#"><img src="{{asset('images/top-banner3.jpg')}}" alt=""></a> </div>
                  <div class="jtv-content-text hidden">
                    <h3 class="title">New Arrival</h3>
                  </div>
                </div>
                <div class="jtv-banner-box banner-inner">
                  <div class="image "> <a class="jtv-banner-opacity" href="#"><img src="{{asset('images/top-banner4.jpg')}}" alt=""></a> </div>
                  <div class="jtv-content-text">
                    <h3 class="title">shoes</h3>
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="jtv-banner-box banner-inner">
                  <div class="image"> <a class="jtv-banner-opacity" href="#"><img src="{{asset('images/top-banner5.jpg')}}" alt=""></a> </div>
                  <div class="jtv-content-text">
                    <h3 class="title">Buy 2 items</h3>
                    <span class="sub-title">get one for free!</span><a href="#" class="button">Shop now!</a> </div>
                </div>
              </div>
              <div class="col-md-5 col-sm-5 col-xs-12">
                <div class="jtv-banner-box">
                  <div class="image"> <a class="jtv-banner-opacity" href="#"><img src="{{asset('images/top-banner2.jpg')}}" alt=""></a> </div>
                  <div class="jtv-content-text">
                    <h3 class="title">perfect light </h3>
                    <span class="sub-title">on brand-new models</span> <a href="#" class="button">Buy Now</a> </div>
                </div>
              </div>
            </div>
          </div>
        </div> --}}
      </div>
    </div>
  </div>
  <div class="container hidden-xs"> 
    <!-- service section -->
    <div class="jtv-service-area">
      <div class="row">
        <div class="col col-md-4 col-sm-4 col-xs-12 no-padding">
          <div class="block-wrapper ship">
            <div class="text-des"> <i class="fa fa-dollar"></i>
              <h3>FREE ESCROW</h3>
              <p>
                Escrow facility (Joint Account) is free of charge
                <br><br>
              </p>
            </div>
          </div>
        </div>
        <div class="col col-md-4 col-sm-4 col-xs-12 no-padding">
          <div class="block-wrapper return">
            <div class="text-des"> <i class="fa fa-lock"></i>
              <h3>SAFETY</h3>
              <p>
                Compare reviews for various trusted online shops throughout Indonesia
              </p>
            </div>
          </div>
        </div>
        <div class="col col-md-4 col-sm-4 col-xs-12 no-padding">
          <div class="block-wrapper support">
            <div class="text-des"> <i class="fa fa-child"></i>
              <h3>TRANSPARENT</h3>
              <p>Your payment is only forwarded to the seller after your item has been received</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- our clients Slider -->
  
  <div class="our-clients hidden-xs">
    <div class="container">
      <div class="slider-items-products">
        <div id="our-clients-slider" class="product-flexslider hidden-buttons">
          <div class="slider-items slider-width-col6"> 
            
            <div class="item"> <a href="#"><img src="https://seeklogo.com/images/T/Tiki_JNE-logo-09BD368D04-seeklogo.com.png" style="height: 50px;" class="grayscale"></a> </div>
            <div class="item"> <a href="#"><img src="https://seeklogo.com/images/T/Tiki-logo-097EE5F63F-seeklogo.com.png" style="height: 50px;" class="grayscale"></a> </div>
            <div class="item"> <a href="#"><img src="https://pngimage.net/wp-content/uploads/2018/06/logo-jnt-png-1.png" style="height: 50px;" class="grayscale"></a> </div>
            <div class="item"> <a href="#"><img src="https://seeklogo.com/images/W/wahana-logistik-logo-9A27D88212-seeklogo.com.png" style="height: 50px;" class="grayscale"></a> </div>
            <div class="item"> <a href="#"><img src="https://seeklogo.com/images/P/pos-indonesia-logo-2DF20234F5-seeklogo.com.png" style="height: 50px;" class="grayscale"></a> </div>
            <div class="item"> <a href="#"><img src="https://seeklogo.com/images/S/sicepat-ekspres-logo-F183B280E5-seeklogo.com.png" style="height: 50px;" class="grayscale"></a> </div>
            <div class="item"> <a href="#"><img src="https://seeklogo.com/images/N/ninja-xpress-logo-860168EFBE-seeklogo.com.png" style="height: 50px;" class="grayscale"></a> </div>
            
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection