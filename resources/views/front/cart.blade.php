@extends('layouts.master')
@section('css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
@endsection
@section('content')
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a title="Go to Home Page" href="{{url('/')}}">Home</a><span>&raquo;</span></li>
            <li><strong>Cart</strong></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
 <!-- Main Container -->
  <section class="main-container col1-layout">
    <div class="main container">
      <div class="col-main">
        <div class="cart">
          
          <div class="page-content page-order"><div class="page-title">
            <h2>Cart</h2>
          </div>
            
            
            <div class="order-detail-content">
              <div class="table-responsive">
                <table class="table table-bordered cart_summary">
                  <thead>
                    <tr>
                      <th class="cart_product">Product</th>
                      <th>Description</th>
                      <th class="text-center">Unit price</th>
                      <th class="text-center">Qty</th>
                      <th class="text-center">Total</th> 
                      <th class="action"><i class="fa fa-trash-o"></i></th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($cart as $data)
                    <tr>
                      <td class="cart_product"><img src="{{asset('images/products/'.\App\FotoProduk::where('id_produk',$data->id_produk)->value('img_thumbnail1'))}}"></td>
                      <td class="cart_description"><p class="product-name"><a href="{{url('product/'.\App\Produk::where('id',$data->id_produk)->value('slug'))}}">{{\App\Produk::where('id',$data->id_produk)->value('name')}}</a></p></td>
                      <td class="price">
                        @if(\App\Produk::where('id',$data->id_produk)->value('diskon') > 0)
                          <span>Rp {{str_replace(',','.',number_format(
                            \App\Produk::where('id',$data->id_produk)->value('harga')-((\App\Produk::where('id',$data->id_produk)->value('diskon')/100)*\App\Produk::where('id',$data->id_produk)->value('harga'))
                          ))}}</span>
                        @else
                          <span>Rp {{str_replace(',','.',number_format(\App\Produk::where('id',$data->id_produk)->value('harga')))}}</span>
                        @endif
                      </td>
                      <td class="qty">{{$data->qty}}</td>
                      <td class="price"><span>Rp {{str_replace(',','.',number_format($data->total))}}</span></td>
                      <td class="action">
                        <a href="{{url('cart/hapus/'.$data->id)}}"><i class="icon-close"></i></a>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
             <tfoot>
                    <tr class="first last">
                      <td colspan="50" class="a-right last">
                        <a href="{{url('/')}}" title="Continue Shopping" class="btn btn-continue"><span>Continue Shopping</span></button>
                    </tr>
                  </tfoot>
                </table>
              </div>
              
            </div>
            
            <div class="cart-collaterals row">
              <form action="{{url('checkout')}}" method="POST">
                {{csrf_field()}}
                <div class="col-sm-4">
                  <div class="shipping">
                    <h3>Estimate Shipping and Tax</h3>
                    <div class="shipping-form">
                      <div id="shipping-zip-form">
                        <p>Enter your destination to get a shipping estimate.</p>
                        <ul class="form-list">
                          <li>
                            <label>State/Province</label>
                            <div class="input-box">
                              <select class="form-control select2" name="province" id="provinsi" title="State/Province">
                                <option value="">Please select region, state or province</option>
                                @forelse($provinsi->rajaongkir->results as $dat_prov)
                                <option value="{{$dat_prov->province}}" data-val="{{$dat_prov->province_id}}">{{$dat_prov->province}}</option>
                                @endforeach
                              </select>
                              @if ($errors->has('province'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('province') }}</strong>
                                  </span>
                              @endif
                            </div>
                          </li>
                          <li>
                            <label>City</label>
                            <div class="input-box">
                              <select class="form-control select2 kotabaru" name="city" id="kota" title="City">
                                <option value="">Please select city</option>
                              </select>
                              @if ($errors->has('city'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('city') }}</strong>
                                  </span>
                              @endif
                            </div>
                          </li>
                          <li>
                            <label>Sub-District</label>
                            <div class="input-box">
                              <select class="select2 kecbaru" name="sub_district" id="kec">
                                <option value="">Pelase select sub-district</option>
                              </select>
                              @if ($errors->has('sub_district'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('sub_district') }}</strong>
                                  </span>
                              @endif
                            </div>
                          </li>
                          <li>
                            <label>Courier</label>
                            <div class="input-box">
                              <select class="select2" name="courier" id="courier" disabled>
                                <option value="">Pelase select courier</option>
                                <option value="jne"> JNE </option> 
                                <option value="pos"> POS </option> 
                                <option value="tiki"> TIKI </option> 
                                <option value="wahana"> WAHANA </option> 
                                <option value="sicepat"> SICEPAT </option> 
                                <option value="jnt"> JNT </option> 
                                <option value="ninja"> NINJA </option> 
                                <option value="jet"> JET </option> 
                              </select>
                              @if($errors->has('courier'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('courier') }}</strong>
                                </span>
                              @endif
                            </div>
                          </li>
                          <li>
                            <label>Shipping Type</label>
                            <div class="input-box">
                              <select class="select2 form-control" name="shipping_type" id="check-shipp" disabled>
                                <option value="">-- Please select type shipping</option>
                              </select>
                              @if ($errors->has('shipping_type'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('shipping_type') }}</strong>
                                  </span>
                              @endif
                            </div>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="shipping-form" id="shipping-zip-form">
                    <h3>Full Address</h3>
                    <ul class="form-list">
                      <li>
                        <label>Name</label>
                        <div class="input-box">
                          <input type="text" class="form-control" name="name" placeholder="Please insert name destination">
                          @if ($errors->has('name'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('name') }}</strong>
                              </span>
                          @endif
                        </div>
                      </li>
                      <li>
                        <label>Email</label>
                        <div class="input-box">
                          <input type="text" class="form-control" name="email" placeholder="Please insert your email">
                          @if ($errors->has('email'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('email') }}</strong>
                              </span>
                          @endif
                        </div>
                      </li>
                      <li>
                        <label>Phone</label>
                        <div class="input-box">
                          <input type="text" class="form-control" name="phone" placeholder="Please insert your phone number">
                          @if ($errors->has('phone'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('phone') }}</strong>
                              </span>
                          @endif
                        </div>
                      </li>
                      <li>
                        <label>Full Address</label>
                        <div class="input-box">
                          <textarea name="address" class="form-control" cols="30" rows="10" placeholder="Please insert your full address for shipping"></textarea>
                          @if ($errors->has('address'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('address') }}</strong>
                              </span>
                          @endif
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="totals col-sm-4">
                  <h3>Shopping Cart Total</h3>
                  <div class="inner">
                    <table id="shopping-cart-totals-table" class="table shopping-cart-table-total">
                      <colgroup>
                      <col>
                      <col width="1">
                      </colgroup>
                      <tbody>
                        <tr>
                          <td style="" class="a-left" colspan="1"> Subtotal </td>
                          <td style="" class="a-right"><span class="price">Rp {{str_replace(',','.',number_format(\App\Cart::where('id_user',Auth::user()->id)->sum('total')))}}</span></td>
                        </tr>
                        <tr>
                          <td style="" class="a-left" colspan="1"> Shipping </td>
                          <td style="" class="a-right">
                            <span class="price shippingnya">
                              <div class="coba-ship">Rp 0</div>
                            </span>
                          </td>
                        </tr>
                      </tbody>
                      <tfoot>
                        <tr>
                          <td style="" class="a-left" colspan="1"><strong>Grand Total</strong></td>
                          <td style="" class="a-right">
                            <strong>
                              <span class="price price-total">
                                <div class="total coba-total">
                                  Rp {{str_replace(',','.',number_format(\App\Cart::where('id_user',Auth::user()->id)->sum('total')))}}
                                </div>
                              </span>
                            </strong>
                          </td>
                        </tr>
                      </tfoot>
                    </table>
                    <ul class="checkout">
                      <li>
                        <button title="Proceed to Checkout" class="button btn-proceed-checkout"><span>Proceed to Checkout</span></button>
                      </li>
                    </ul>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection
@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script>
  $('.select2').select2();
  $('#provinsi').change(function(){          
      var provinsinya = $(this).find(':selected').attr('data-val');
      $.ajax({
        type : 'POST',
        url  : '{{url('cek_kota')}}',
        data : {
          provinsi : $(this).find(':selected').attr('data-val'),
          _token : '{{csrf_token()}}'
        },  
        beforeSend: function() {
            $('.coba').remove();
        },
        success: function(response){
          if($.isArray(response.data.post)){
            $.each(response.data.post, function(key, value){
                if (value.type == 'Kota') {
                    var pender = '<option class="coba" data-val="'+value.city_id+'" value="'+value.city_name+'">'+value.city_name+'</option>';
                    $('#kota').append(pender);
                }
                var input = '<input type="hidden" class="coba" name="id_provinsi" value="'+provinsinya+'">';
                $('#provinsi').append(input);
            });
          }
        }
      });
  });
  $('#kota').change(function(){
    var citynya = $(this).find(':selected').attr('data-val');
    $.ajax({
      type : 'POST',
      url  : '{{url('cek_kecamatan')}}',
      data : {
        id_city : $(this).find(':selected').attr('data-val'),
        _token  : '{{csrf_token()}}'
      },  
      beforeSend: function() {
          $('.coba_kec').remove();
      },
      success: function(response){
        if($.isArray(response.data.post)){
          $.each(response.data.post, function(key, value){
              var pender = '<option class="coba_kec" data-val="'+value.subdistrict_id+'" value="'+value.subdistrict_name+'">'+value.subdistrict_name+'</option>';
              $('#kec').append(pender);
              var input = '<input type="hidden" class="coba_kec" name="id_city" value="'+citynya+'">'+
                          '<input type="hidden" class="coba_kec" name="id_kec" value="'+value.subdistrict_id+'">';
              $('#kota').append(input);
          });
        }
      }
    });
});
$('.kecbaru').change(function(){
  $('#courier').removeAttr('disabled');
})
$('#courier').change(function(){
  $.ajax({
      type : 'POST',
      url  : '{{url('cek_courier')}}',
      data : {
        courier  : $(this).find(':selected').val(),
        id_city  : $('#kota').find(':selected').attr('data-val'),
        provinsi : $('#provinsi').find(':selected').attr('data-val'),
        subdistrict : $('.kecbaru').find(':selected').attr('data-val'),
        _token   : '{{csrf_token()}}'
      },
      beforeSend :function(){
        $('.checking').remove();
        $('#check-shipp').removeAttr('disabled');
        $('.cobanya').remove();
      },
      success : function(response){
        $.each(response.data.post, function(k,v){
          var shipment = '<option class="cobanya" data-val="'+v.val+'" data-price="'+v.price+'" value="'+v.service+'">'+v.service+' ( '+v.description+')</option>';
          $('#check-shipp').append(shipment);
        });
        $('#check-shipp').change(function(){
          $('.coba-ship').remove();
          $('.coba-total').remove();
          $('.shippingnya').append('<div class="coba-ship">Rp '+$(this).find(':selected').data('price')+'</div>');
          var total = '{{\App\Cart::where('id_user',Auth::user()->id)->sum('total')}}';
          var ship  = $(this).find(':selected').data('val');
          $('.price-total').append('<div class="coba-total">Rp '+(Number(total)+Number(ship))+'</div>');
        })
      }
    });
});  
</script>
@endsection