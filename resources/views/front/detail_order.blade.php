@extends('layouts.master')
@section('content')
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a title="Go to Home Page" href="{{url('/')}}">Home</a><span>&raquo;</span></li>
            <li class="home"> <a title="My Order" href="{{url('myorder')}}">My Order</a><span>&raquo;</span></li>
            <li><strong>Detail Order</strong></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
<!-- Main Container -->
  <section class="main-container col2-left-layout">
    <div class="main container">
      <div class="row">
        <aside class="left sidebar col-sm-3 col-xs-12">
          <div class="sidebar-account block">
            <div class="sidebar-bar-title">
              <h3>My Account</h3>
            </div>
            <div class="block-content">
              @include('include.sidebar-member')
            </div>
          </div>
        </aside>
        <div class="col-main col-sm-9 col-xs-12">
          <div class="my-account">
            <div class="page-title">
              <h2>Detail Order</h2>
            </div>
            <table class="table table-striped table-bordered">
              <tr>
                <th colspan="3" class="text-center" style="background:#34495e; color:#fff;">SHIPPING</th>
              </tr>
              <tr>
                <td width="25%">Order Code</td>
                <td class="text-center"  width="5%">:</td>
                <td>{{$trans->kode_order}}</td>
              </tr>
              <tr>
                <td width="25%">Status</td>
                <td class="text-center"  width="5%">:</td>
                <td>
                  @if($trans->status == 0)
                      <label for="" class="label label-warning">Order has not been paid</label>
                  @elseif($trans->status == 1)
                      <label for="" class="label label-primary">Order Has Been Paid</label>
                  @elseif($trans->status == 2)
                      <label for="" class="label label-success">Order Has Been Sent to Shippment</label>
                  @elseif($trans->status == 3)
                      <label for="" class="label label-success">Order Already Shippment</label>
                  @elseif($trans->status == 4)
                      <label for="" class="label label-danger">Payment Has Been Rejected</label>
                  @endif
                </td>
              </tr>
              <tr>
                <td width="25%">No Resi</td>
                <td class="text-center"  width="5%">:</td>
                <td>{{$trans->noresi}}</td>
              </tr>
              <tr>
                <td width="25%">Name</td>
                <td class="text-center"  width="5%">:</td>
                <td>{{$trans->nama}}</td>
              </tr>
              <tr>
                <td width="25%">Email</td>
                <td class="text-center"  width="5%">:</td>
                <td>{{$trans->email}}</td>
              </tr>
              <tr>
                <td width="25%">Phone</td>
                <td class="text-center"  width="5%">:</td>
                <td>{{$trans->telp}}</td>
              </tr>
              <tr>
                <td width="25%">Province</td>
                <td class="text-center"  width="5%">:</td>
                <td>{{$trans->provinsi}}</td>
              </tr>
              <tr>
                <td>City</td>
                <td class="text-center" >:</td>
                <td>{{$trans->kota}}</td>
              </tr>
              <tr>
                <td>Sub District</td>
                <td class="text-center" >:</td>
                <td>{{$trans->kecamatan}}</td>
              </tr>
              <tr>
                <td>Full Address</td>
                <td class="text-center" >:</td>
                <td>{{$trans->alamat}}</td>
              </tr>
              <tr>
                <td>Courier</td>
                <td class="text-center" >:</td>
                <td>{{$trans->kurir}}</td>
              </tr>
              <tr>
                <td>Shipping Type</td>
                <td class="text-center" >:</td>
                <td>{{$trans->shipping_desc}}</td>
              </tr>
              <tr>
                <td>Shipping Estimate</td>
                <td class="text-center" >:</td>
                <td>{{$trans->etd}}</td>
              </tr>
              <tr>
                <td>Shipping Price</td>
                <td class="text-center" >:</td>
                <td>Rp {{str_replace(',','.',number_format($trans->shipping_price))}}</td>
              </tr>
            </table>
            <table class="table table-striped table-bordered">
              <tr>
                <th colspan="4" class="text-center" style="background:#34495e; color:#fff;">PRODUCTS</th>
              </tr>
              <tr>
                <td>Name</td>
                <td>Image</td>
                <td>QTY</td>
                <td>Price</td>
              </tr>
              @foreach($pro as $data)
              <tr>
                <td>{{\App\Produk::where('id',$data->id_produk)->value('name')}}</td>
                <td><img class="img-responsive" src="{{asset('images/products/'.\App\FotoProduk::where('id_produk',$data->id_produk)->value('img_thumbnail1'))}}" alt=""></td>
                <td>{{$data->qty}}</td>
                <td>Rp {{str_replace(',','.',number_format($data->total))}}</td>
              </tr>
              @endforeach
              <tr>
                <td colspan="3">Sub Total</td>
                <td>Rp {{str_replace(',','.',number_format($pro->sum('total')))}}</td>
              </tr>
              <tr>
                <td colspan="3">Shipping</td>
                <td>Rp {{str_replace(',','.',number_format($trans->shipping_price))}}</td>
              </tr>
              <tr>
                <td colspan="3">Grand Total</td>
                <td>Rp {{str_replace(',','.',number_format($pro->sum('total')+$trans->shipping_price))}}</td>
              </tr>
            </table>
            @if($trans->status == 0 || $trans->status == 4)
            <a data-toggle="modal" data-target="#checkout" title="Proceed to Payment" class="button btn-proceed-checkout btn-block text-center" style="background: #1abc9c; color: #fff;"><span>Proceed to Payment</span></a>
            @endif
          </div>
        </div>
      </div>
    </div>
  </section>
   <!-- our clients Slider -->
   @if($trans->status == 0 || $trans->status == 4)
   <div id="checkout" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Confirm Your Payment</h4>
          </div>
          <div class="modal-body">
            <form action="{{url('checkout-payment/'.$trans->kode_order)}}" method="POST" enctype="multipart/form-data">
              {{csrf_field()}}
              <div class="form-group">
                <label>Order Code <sup style="color: red;">*</sup></label>
                <input type="text" class="form-control" value="{{$trans->kode_order}}" disabled>
              </div>
              <div class="form-group">
                <label>Account Bank Name <sup style="color: red;">*</sup></label>
                <input type="text" class="form-control" name="account_name" placeholder="Please insert your Account Bank Name" required>
                @if ($errors->has('account_name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('account_name') }}</strong>
                    </span>
                @endif
              </div>
              <div class="form-group">
                <label>Bank Name <sup style="color: red;">*</sup></label>
                <input type="text" class="form-control" name="bank_name" placeholder="Please insert your Bank Name" required>
                @if ($errors->has('bank_name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('bank_name') }}</strong>
                    </span>
                @endif
              </div>
              <div class="form-group">
                <label>Upload Bank Receipt <sup style="color: red;">*</sup></label>
                <input type="file" class="form-control" name="bank_receipt" required>
                @if ($errors->has('bank_receipt'))
                    <span class="help-block">
                        <strong>{{ $errors->first('bank_receipt') }}</strong>
                    </span>
                @endif
              </div>
              <div class="form-group">
                <button title="Proceed to Payment" class="button btn-proceed-checkout btn-block text-center"><span>Send</span></button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    @endif
@endsection