@extends('layouts.master')
@section('content')
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a title="Go to Home Page" href="{{url('/')}}">Home</a><span>&raquo;</span></li>
            <li class=""><a title="Kategori" href="{{url('product/cat/'.\App\Kategori::where('id',$pro->id_kategori)->value('slug'))}}">{{\App\Kategori::where('id',$pro->id_kategori)->value('name')}}</a><span>&raquo;</span></li>
            @if(!empty($subnya))
              <li class=""> <a href="{{url('product/cat/'.\App\Kategori::where('id',$subnya->id_kategori)->value('slug'))}}">{{\App\Kategori::where('id',$subnya->id_kategori)->value('name')}}</a><span>&raquo;</span></li>
            @endif
            <li><strong>{{$pro->name}}</strong></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- Main Container -->
  <div class="main-container col2-left-layout">
    <div class="container">
      <div class="row">
        <div class="col-main col-sm-9 col-xs-12 col-sm-push-3">
          <div class="product-view-area">
            <div class="product-big-image col-xs-12 col-sm-5 col-lg-5 col-md-5">
              <div class="icon-sale-label sale-left">Sale</div>
              <div class="large-image"> 
              <a href="{{asset('images/products/'.\App\FotoProduk::where('id_produk',$pro->id)->value('img_full'))}}" class="cloud-zoom" id="zoom1" rel="useWrapper: false, adjustY:0, adjustX:20"> 
                <img class="zoom-img" src="{{asset('images/products/'.\App\FotoProduk::where('id_produk',$pro->id)->value('img_full'))}}" alt="products"> 
              </a> 
              </div>
              <div class="slider-items-products col-md-12">
              <div id="thumbnail-slider1" class="product-flexslider hidden-buttons product-thumbnail">
                <div class="slider-items slider-width-col3">
                  @foreach($foto as $dat_foto)
                  <div class="thumbnail-items"><a href='{{asset('images/products/'.$dat_foto->img_thumbnail1)}}' class='cloud-zoom-gallery' rel="useZoom: 'zoom1', smallImage: 'images/products/'{{$dat_foto->img_thumbnail1}} "><img src="{{asset('images/products/'.$dat_foto->img_thumbnail2)}}"/></a></div>
                  @endforeach
                </div>
              </div>
            </div>
            </div>
            <div class="col-xs-12 col-sm-7 col-lg-7 col-md-7 product-details-area">
              <div class="product-name">
                <h2>{{$pro->name}}</h2>
              </div>
              <br>
              <div class="price-box">
                @if($pro->diskon > 0)
                  <p class="special-price"> 
                    <span class="price-label">Special Price</span> 
                    <span class="price"> Rp {{str_replace(',','.',number_format($pro->harga-(($pro->harga/100)*$pro->diskon)))}} </span> 
                  </p>
                  <p class="old-price"> 
                    <span class="price-label">Regular Price:</span>
                     <span class="price"> Rp {{str_replace(',','.',number_format($pro->harga))}} </span> 
                  </p>
                  @else
                  <span class="regular-price"> 
                    <span class="price-label">Regular Price:</span>
                    <span class="price">Rp {{str_replace(',','.',number_format($pro->harga))}}</span> 
                  </span>
                  @endif
              </div>
              <div class="ratings">
                <p class="availability in-stock pull-right">Availability: <span style="padding: 5px;">In Stock</span></p>
              </div>
              <div class="product-variation">
                <form action="{{url('cart')}}" method="POST">
                  {{csrf_field()}}
                  <div class="cart-plus-minus">
                    <label for="qty">Qty:</label>
                    <div class="numbers-row">
                      <div onClick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty ) &amp;&amp; qty &gt; 0 ) result.value--;return false;" class="dec qtybutton"><i class="fa fa-minus">&nbsp;</i></div>
                      <input type="text" class="qty" title="Qty" value="1" maxlength="12" id="qty" name="qty">
                      <input type="hidden" value="{{$pro->id}}" name="produk">
                      <div onClick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty )) result.value++;return false;" class="inc qtybutton"><i class="fa fa-plus">&nbsp;</i></div>
                      @if ($errors->has('qty'))
                        <br><br>
                        <span class="help-block">
                            <strong>{{ $errors->first('qty') }}</strong>
                        </span>
                      @endif
                      @if ($errors->has('produk'))
                        <br><br>
                        <span class="help-block">
                            <strong>{{ $errors->first('produk') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>
                  <button class="button pro-add-to-cart" title="Add to Cart"><span><i class="fa fa-shopping-cart"></i> Add to Cart</span></button>
                </form>
              </div>
            </div>
          </div>
          <div class="product-overview-tab">
            <div class="product-tab-inner">
              <ul id="product-detail-tab" class="nav nav-tabs product-tabs">
                <li class="active"> <a href="#description" data-toggle="tab"> Description </a> </li>
              </ul>
              <div id="productTabContent" class="tab-content">
                <div class="tab-pane fade in active" id="description">
                  <div class="std">
                    {!! $pro->description !!}
                  </div>
                </div>
              </div>
            </div>
        </div>
        </div>
        <aside class="sidebar hidden-xs col-sm-3 col-xs-12 col-sm-pull-9">
          <div class="block shop-by-side">
            <div class="sidebar-bar-title">
              <h3>Categories</h3>
            </div>
            <div class="block-content">
              <div class="layered-Category">
                <div class="layered-content">
                  <ul class="check-box-list">
                    @foreach($kat as $a => $dat_kat)
                    <li>
                      <a href="{{url('product/cat/'.$dat_kat->slug)}}">{{$dat_kat->name}} <span class="count">({{\App\Produk::where('id_kategori',$dat_kat->id)->where('aktif',1)->count()}})</span></a> 
                      @if(\App\SubKategori::where('id_kategori',$dat_kat->id)->count() > 0)
                        <a class="pull-right" data-toggle="collapse" href="#collapse{{$a+1}}"><i class="fa fa-chevron-down"></i></a>
                        <div id="collapse{{$a+1}}" class="panel-collapse collapse">
                          <ul class="sub" style="margin-left: 10px;">
                            @foreach(\App\SubKategori::where('id_kategori',$dat_kat->id)->get() as $dat_sub)
                              <li>
                                <a href="{{url('product/'.$dat_kat->slug.'/'.$dat_sub->slug)}}">- {{$dat_sub->name}} <span class="count">({{\App\Produk::where('id_subkategori',$dat_sub->id)->where('aktif',1)->count()}})</span></a>
                              </li>
                            @endforeach
                          </ul>
                        </div>
                      @endif
                    </li>
                    @endforeach
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="single-img-add sidebar-add-slider ">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel"> 
              <!-- Indicators -->
              <ol class="carousel-indicators">
                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
              </ol>
              
              <!-- Wrapper for slides -->
              <div class="carousel-inner" role="listbox">

                <div class="item active"> 
                  <img src="http://laz-img-cdn.alicdn.com/images/ims-web/TB1tzBdUNYaK1RjSZFnXXa80pXa.png_240x240Q100.jpg_.webp" alt="slide1">
                  <div class="carousel-caption">
                    <h3><a href="single_product.html" title=" Sample Product">Sale Up to 50% off</a></h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    <a href="#" class="info">shopping Now</a> </div>
                </div>
                <div class="item"> 
                  <img src="{{asset('images/add-slide2.jpg')}}" alt="slide2">
                  <div class="carousel-caption">
                    <h3><a href="single_product.html" title=" Sample Product">Smartwatch Collection</a></h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    <a href="#" class="info">All Collection</a> </div>
                </div>
                <div class="item"> 
                  <img src="https://www.static-src.com/siva/asset//05_2019/Marvel2.jpg?output-format=webp" alt="slide3">
                  <div class="carousel-caption">
                    <h3><a href="single_product.html" title=" Sample Product">Summer Sale</a></h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                  </div>
                </div>
              </div>
              
              <!-- Controls --> 
              <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next"> <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> <span class="sr-only">Next</span> </a> </div>
          </div>
          {{-- <div class="block popular-tags-area ">
            <div class="sidebar-bar-title">
              <h3>Popular Tags</h3>
            </div>
            <div class="tag">
              <ul>
                <li><a href="#">Boys</a></li>
                <li><a href="#">Camera</a></li>
                <li><a href="#">good</a></li>
                <li><a href="#">Computers</a></li>
                <li><a href="#">Phone</a></li>
                <li><a href="#">clothes</a></li>
                <li><a href="#">girl</a></li>
                <li><a href="#">shoes</a></li>
                <li><a href="#">women</a></li>
                <li><a href="#">accessoties</a></li>
                <li><a href="#">View All Tags</a></li>
              </ul>
            </div>
          </div> --}}
        </aside>
      </div>
    </div>
  </div>
  <!-- Main Container End --> 
  @if(count($related) > 0)
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <div class="related-product-area">
          <div class="title_block">
            <h3 class="products_title">Related Products</h3>
          </div>
          <div class="related-products-pro">
            <div class="slider-items-products">
              <div id="related-product-slider" class="product-flexslider hidden-buttons">
                <div class="slider-items slider-width-col4">
                  @foreach($related as $dat_related)
                      <div class="product-item">
                      <div class="item-inner">
                        <div class="product-thumbnail">
                          <div class="icon-sale-label sale-left">Sale</div>
                          <a href="{{url('product/'.$dat_related->slug)}}" class="product-item-photo"> <img class="product-image-photo" src="{{asset('images/products/'.\App\FotoProduk::where('id_produk',$dat_related->id)->value('img_full'))}}" alt=""></a> </div>
                          <div class="pro-box-info">
                            <div class="item-info">
                              <div class="info-inner">
                                <div class="item-title">
                                  <h4> <a title="{{title_case($dat_related->name)}}" href="{{url('product/'.$dat_related->slug)}}">{{title_case($dat_related->name)}} </a></h4>
                                </div>
                                <div class="item-content">
                                  {{-- <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div> --}}
                                  <div class="item-price">
                                    <div class="price-box"> 
                                      @if($dat_related->diskon > 0)
                                        <p class="special-price"> 
                                          <span class="price-label">Special Price</span> 
                                          <span class="price"> Rp {{str_replace(',','.',number_format($dat_related->harga-(($dat_related->harga/100)*$dat_related->diskon)))}} </span> 
                                        </p>
                                        <p class="old-price"> 
                                          <span class="price-label">Regular Price:</span>
                                           <span class="price"> Rp {{str_replace(',','.',number_format($dat_related->harga))}} </span> 
                                        </p>
                                        @else
                                        <span class="regular-price"> 
                                          <span class="price-label">Regular Price:</span>
                                          <span class="price">Rp {{str_replace(',','.',number_format($dat_related->harga))}}</span> 
                                        </span>
                                        @endif
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="box-hover">
                              <div class="product-item-actions">
                                <div class="pro-actions">
                                  <a href="{{url('product/'.$dat_related->slug)}}"  class="action add-to-cart button-cart" title="View Detail"> <span>View Detail</span> </a>
                                </div>
                              </div>
                            </div>
                          </div>
                      </div>
                      </div>
                    @endforeach
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endif
  <!-- Related Product Slider End --> 
@endsection