@extends('layouts.master')
@section('content')
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a title="Go to Home Page" href="{{url('/')}}">Home</a><span>&raquo;</span></li>
            <li><strong>Billing Agreements</strong></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
<!-- Main Container -->
  <section class="main-container col2-left-layout">
    <div class="main container">
      <div class="row">
        <aside class="left sidebar col-sm-3 col-xs-12">
          <div class="sidebar-account block">
            <div class="sidebar-bar-title">
              <h3>My Account</h3>
            </div>
            <div class="block-content">
              @include('include.sidebar-member')
            </div>
          </div>
        </aside>
        <div class="col-main col-sm-9 col-xs-12">
          <div class="my-account">
            <div class="page-title">
              <h2>Billing Agreements</h2>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
   <!-- our clients Slider -->
@endsection