<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">

        <title>{{\App\Setting::where('id',1)->value('nama_toko')}} - Administrator</title>

        <meta name="description" content="Login Administrator Toko Online">
        <meta name="author" content="indowebremote">
        <meta name="robots" content="index,follow">

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <!-- Icons -->
        <link rel="shortcut icon" href="img/favicon.png">
        <!-- END Icons -->
        <link rel="stylesheet" href="{{asset('admin/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('admin/css/plugins.css')}}">
        <link rel="stylesheet" href="{{asset('admin/css/main.css')}}">
        <link rel="stylesheet" href="{{asset('admin/css/themes.css')}}">
        <script src="{{asset('admin/js/vendor/modernizr.min.js')}}"></script>
        @yield('css')
    </head>
    <body>
        <div id="page-wrapper">
            <div id="page-container" class="sidebar-partial sidebar-visible-lg sidebar-no-animations">
                @include('include.sidebar-admin')
                <div id="main-container">
                    <!-- Header -->
                    <header class="navbar navbar-default">
                        <ul class="nav navbar-nav-custom">
                            <li>
                                <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar');this.blur();">
                                    <i class="fa fa-bars fa-fw"></i>
                                </a>
                            </li>
                        </ul>
                    </header>
                    <!-- END Header -->
                    @yield('content')
                    <!-- Footer -->
                    <footer class="clearfix">
                        <div class="pull-right">
                            Adminpanel by <a href="http://indowebremote" target="_blank">indowebremote.com</a>
                        </div>
                    </footer>
                    <!-- END Footer -->
                </div>
            </div>
        </div>

        <script src="{{asset('admin/js/vendor/jquery.min.js')}}"></script>
        <script src="{{asset('admin/js/vendor/bootstrap.min.js')}}"></script>
        {{-- <script src="{{asset('admin/js/plugins.js')}}"></script> --}}
        <script src="{{asset('admin/js/app.js')}}"></script>
        @include('include.alert')
        @yield('js')
    </body>
</html>