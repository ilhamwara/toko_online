<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>{{$set->nama_toko}}</title>
<meta name="description" content="Toko Online terbaik dan terpercaya support payment gateway">
<meta name="keywords" content="toko online,e commerce,toko"/>

<!-- Mobile specific metas  -->
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Favicon  -->
<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">

<!-- CSS Style -->
<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('css/animate.css')}}">
<link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{asset('css/simple-line-icons.css')}}">
<link rel="stylesheet" href="{{asset('css/meanmenu.min.css')}}">
<link rel="stylesheet" href="{{asset('css/owl.carousel.css')}}">
<link rel="stylesheet" href="{{asset('css/owl.transitions.css')}}">
<link rel="stylesheet" href="{{asset('css/nivo-slider.css')}}">
<link rel="stylesheet" href="{{asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{asset('css/blog.css')}}">
<link rel="stylesheet" href="{{asset('css/slick.min.css')}}">
<link rel="stylesheet" href="{{asset('css/style.css')}}">
<link rel="stylesheet" href="{{asset('css/responsive.css')}}">
@yield('css')

</head>

<body class="cms-index-index cms-home-page">
<div id="page"> 
  @include('include.header')
  @yield('content')
  @include('include.footer')
</div>

<!-- JS --> 
<!-- jquery js --> 
<script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script> 
<!-- bootstrap js --> 
<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script> 

<!-- Mean Menu js --> 
<script type="text/javascript" src="{{asset('js/jquery.meanmenu.min.js')}}"></script> 

<!-- owl.carousel.min js --> 
<script type="text/javascript" src="{{asset('js/owl.carousel.min.js')}}"></script> 

<!--jquery-ui.min js --> 
<script type="text/javascript" src="{{asset('js/jquery-ui.js')}}"></script> 

<!-- countdown js --> 
<script type="text/javascript" src="{{asset('js/countdown.js')}}"></script> 

<!-- wow JS --> 
<script type="text/javascript" src="{{asset('js/wow.min.js')}}"></script> 

<!-- main js --> 
<script type="text/javascript" src="{{asset('js/main.js')}}"></script> 

<!-- nivo slider js --> 
<script type="text/javascript" src="{{asset('js/jquery.nivo.slider.js')}}"></script> 
@yield('js')
@include('include.alert')
</body>
</html>
