  <!-- Footer -->
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-sm-6 col-md-4 col-xs-12 col-lg-3">
          <div class="logo"><a href="index-2.html"><h2>{{$set->nama_toko}}</h2></a> </div>
          <div class="footer-content">
            <div class="email"> <i class="fa fa-envelope"></i>
              <p>{{$set->email}}</p>
            </div>
            <div class="phone"> <i class="fa fa-phone"></i>
              <p>{{$set->phone}}</p>
            </div>
            <div class="address"> <i class="fa fa-map-marker"></i>
              <p> {{$set->alamat}}</p>
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-3 col-xs-12 col-lg-3 collapsed-block">
          <div class="footer-links">
            <h3 class="links-title">Information<a class="expander visible-xs" href="#TabBlock-1">+</a></h3>
            <div class="tabBlock" id="TabBlock-1">
              <ul class="list-links list-unstyled">
                @foreach($menu_info as $info)
                  <li><a href="{{url('page/'.$info->link)}}">{{$info->nama}}</a></li>
                @endforeach
              </ul>
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-3 col-xs-12 col-lg-3 collapsed-block">
          <div class="footer-links">
            <h3 class="links-title">Insider<a class="expander visible-xs" href="#TabBlock-3">+</a></h3>
            <div class="tabBlock" id="TabBlock-3">
              <ul class="list-links list-unstyled">
                <li><a href="">About Us</a></li>
                <li><a href="">Contact Us</a></li>
                <li><a href="">My Account</a></li>
                @foreach($menu_insider as $insider)
                  <li><a href="{{url('page/'.$insider->link)}}">{{$insider->nama}}</a></li>
                @endforeach
              </ul>
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-2 col-xs-12 col-lg-3 collapsed-block">
          <div class="footer-links">
            <h3 class="links-title">Service<a class="expander visible-xs" href="#TabBlock-4">+</a></h3>
            <div class="tabBlock" id="TabBlock-4">
              <ul class="list-links list-unstyled">
                @foreach($menu_service as $service)
                  <li><a href="{{url('page/'.$service->link)}}">{{$service->nama}}</a></li>
                @endforeach
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-newsletter">
      <div class="container">
        <div class="row">
          <div class="col-md-3 col-sm-5">
            <h3 class="">Sign up for newsletter</h3>
            <span>Get the latest deals and special offers</span></div>
          <div class="col-md-5 col-sm-7">
            <form id="newsletter-validate-detail" method="post" action="#">
              <div class="newsletter-inner">
                <input class="newsletter-email" name='Email' placeholder='Enter Your Email'/>
                <button class="button subscribe" type="submit" title="Subscribe">Subscribe</button>
              </div>
            </form>
          </div>
          <div class="col-md-4 col-sm-12">
            <div class="social">
              <ul class="inline-mode">
                <li class="social-network fb"><a title="Connect us on Facebook" target="_blank" href="https://www.facebook.com/"><i class="fa fa-facebook"></i></a></li>
                <li class="social-network tw"><a title="Connect us on Twitter" target="_blank" href="https://twitter.com/"><i class="fa fa-twitter"></i></a></li>
                <li class="social-network instagram"><a title="Connect us on Instagram" target="_blank" href="https://instagram.com/"><i class="fa fa-instagram"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-coppyright">
      <div class="container">
        <div class="row">
          <div class="col-sm-6 col-xs-12 coppyright"> Copyright © {{date('Y')}} <a href="#"> Toko Online </a>. All Rights Reserved. </div>
          <div class="col-sm-6 col-xs-12">
            <div class="payment">
              <ul>
                <li><a href="#"><img title="Visa" alt="Visa" src="{{asset('images/visa.png')}}"></a></li>
                <li><a href="#"><img title="Paypal" alt="Paypal" src="{{asset('images/paypal.png')}}"></a></li>
                <li><a href="#"><img title="Discover" alt="Discover" src="{{asset('images/discover.png')}}"></a></li>
                <li><a href="#"><img title="Master Card" alt="Master Card" src="{{asset('images/master-card.png')}}"></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <a href="#" class="totop"> </a> 
  <!-- End Footer -->