<!-- Main Sidebar -->
<div id="sidebar">
    <!-- Wrapper for scrolling functionality -->
    <div id="sidebar-scroll">
        <!-- Sidebar Content -->
        <div class="sidebar-content">
            <!-- Brand -->
            <a href="#" class="sidebar-brand">
                <i></i><span class="sidebar-nav-mini-hide"><strong>Administrator</strong></span>
            </a>
            <!-- END Brand -->

            <!-- User Info -->
            <div class="sidebar-section sidebar-user clearfix sidebar-nav-mini-hide">
                <div class="sidebar-user-avatar">
                    <a href="page_ready_user_profile.html">
                        <img src="{{asset('admin/img/placeholders/avatars/avatar2.jpg')}}" alt="avatar">
                    </a>
                </div>
                <div class="sidebar-user-name">{{Auth::user()->name}}</div>
                <div class="sidebar-user-links">
                    <a href="{{url('logout')}}" data-toggle="tooltip" data-placement="bottom" title="Logout"><i class="gi gi-exit"></i></a>
                </div>
            </div>
            <!-- END User Info -->

            <!-- Sidebar Navigation -->
            <ul class="sidebar-nav">
                <li>
                    <a href="{{url('/')}}" target="_blank"><i class="fa fa-eye sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">View Site</span></a>
                </li>
                <li @if(Request::is('administrator/dashboard*')) class="active" @endif>
                    <a @if(Request::is('administrator/dashboard*')) class="active" @endif href="{{url('administrator/dashboard')}}"><i class="fa fa-dashboard sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Dashboard</span></a>
                </li>
                <li @if(Request::is('administrator/page*')) class="active" @endif>
                    <a @if(Request::is('administrator/page*')) class="active" @endif href="{{url('administrator/page')}}"><i class="fa fa-bookmark sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Page</span></a>
                </li>
                <li @if(Request::is('administrator/menu*')) class="active" @endif>
                    <a @if(Request::is('administrator/menu*')) class="active" @endif href="{{url('administrator/menu')}}"><i class="fa fa-share-alt sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Setting Menu</span></a>
                </li>
                <li @if(Request::is('administrator/banner*')) class="active" @endif>
                    <a @if(Request::is('administrator/banner*')) class="active" @endif href="{{url('administrator/banner')}}"><i class="fa fa-picture-o sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Banner</span></a>
                </li>
                {{-- <li @if(Request::is('administrator/article*')) class="active" @endif>
                    <a @if(Request::is('administrator/article*')) class="active" @endif href="{{url('administrator/article')}}"><i class="fa fa-newspaper-o sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Article</span></a>
                </li> --}}
                <li @if(Request::is('administrator/categories*')) class="active" @endif>
                    <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="fa fa-level-down sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide"> Categories</span></a>
                    <ul>
                        <li><a @if(Request::is('administrator/categories/list*')) class="active" @endif href="{{url('administrator/categories/list')}}">Categories</a></li>
                        <li><a @if(Request::is('administrator/categories/sub*')) class="active" @endif href="{{url('administrator/categories/sub')}}">Sub Categories</a></li>
                    </ul>
                </li>
                <li @if(Request::is('administrator/products*')) class="active" @endif>
                    <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="fa fa-archive sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide"> Products</span></a>
                    <ul>
                        <li><a @if(Request::is('administrator/products/add*')) class="active" @endif href="{{url('administrator/products/add')}}">Add New Product</a></li>
                        <li><a @if(Request::is('administrator/products/list*')) class="active" @endif href="{{url('administrator/products/list')}}">All Products</a></li>
                        <li><a @if(Request::is('administrator/products/featured*')) class="active" @endif href="{{url('administrator/products/featured')}}">Featured</a></li>
                        <li><a @if(Request::is('administrator/products/new_arrival*')) class="active" @endif href="{{url('administrator/products/new_arrival')}}">New Arrival</a></li>
                        <li><a @if(Request::is('administrator/products/promo*')) class="active" @endif href="{{url('administrator/products/promo')}}">Promo</a></li>
                        <li><a @if(Request::is('administrator/products/flash_sale*')) class="active" @endif href="{{url('administrator/products/flash_sale')}}">Flash Sale</a></li>
                    </ul>
                </li>
                {{-- <li @if(Request::is('administrator/coupon*')) class="active" @endif>
                    <a @if(Request::is('administrator/coupon*')) class="active" @endif href="{{url('administrator/coupon')}}"><i class="fa fa-star sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Coupon</span></a>
                </li> --}}
                <li @if(Request::is('administrator/order*')) class="active" @endif>
                    <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="fa fa-shopping-bag sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Orders</span></a>
                    <ul>
                        <li><a @if(Request::is('administrator/order')) class="active" @endif href="{{url('administrator/order')}}">All Orders</a></li>
                        <li>
                            <a @if(Request::is('administrator/order/pending*')) class="active" @endif href="{{url('administrator/order/pending')}}">Order Pending 
                                @if(\App\Transaksi::where('status',1)->count() > 0)
                                <span class="badge" style="background:#e74c3c; color: #fff;">{{\App\Transaksi::where('status',1)->count()}}</span>
                                @endif
                            </a>
                        </li>
                        <li>
                            <a @if(Request::is('administrator/order/confirm')) class="active" @endif href="{{url('administrator/order/confirm')}}">Order Confirm Payment
                                @if(\App\Transaksi::where('status',2)->count() > 0)
                                    <span class="badge" style="background:#e74c3c; color: #fff;">{{\App\Transaksi::where('status',2)->count()}}</span>
                                @endif
                            </a>
                        </li>
                        <li>
                            <a @if(Request::is('administrator/order/confirm-shipping*')) class="active" @endif href="{{url('administrator/order/confirm-shipping')}}">Order Confirm Shipping
                                @if((\App\Transaksi::where('status',3)->where('noresi','')->count() > 0))
                                    <span class="badge" style="background:#e74c3c; color: #fff;">{{\App\Transaksi::where('status',3)->where('noresi','')->count()}}</span>
                                @endif
                            </a>
                        </li>
                        <li><a @if(Request::is('administrator/order/rejected*')) class="active" @endif href="{{url('administrator/order/rejected')}}">Order Rejected</a></li>
                    </ul>
                </li>
                {{-- <li @if(Request::is('administrator/clients*')) class="active" @endif>
                    <a @if(Request::is('administrator/clients*')) class="active" @endif href="{{url('administrator/clients')}}"><i class="fa fa-handshake-o sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Clients</span></a>
                </li> --}}
                <li @if(Request::is('administrator/setting_site*')) class="active" @endif>
                    <a @if(Request::is('administrator/setting_site*')) class="active" @endif href="{{url('administrator/setting_site')}}"><i class="fa fa-cogs sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Setting Site</span></a>
                </li>
                {{-- <li @if(Request::is('administrator/users*')) class="active" @endif>
                    <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="fa fa-users sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Management User</span></a>
                    <ul>
                        <li><a @if(Request::is('administrator/users*')) class="active" @endif href="{{url('administrator/users/add')}}">Add Users</a></li>
                        <li><a @if(Request::is('administrator/users*')) class="active" @endif href="{{url('administrator/users/member')}}">List Member</a></li>
                        <li><a @if(Request::is('administrator/users*')) class="active" @endif href="{{url('administrator/users/admin')}}">List Admin</a></li>
                    </ul>
                </li> --}}
                <li>
                    <a href="{{url('logout')}}"><i class="gi gi-exit sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Logout</span></a>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- END Main Sidebar -->