<ul>
    <li class="@if(Request::is('memberarea')) current @endif"><a href="{{url('memberarea')}}">Account Information</a></li>
    {{-- <li class="@if(Request::is('address_book')) current @endif"><a href="{{url('address_book')}}">Address Book</a></li> --}}
    <li class="@if(Request::is('myorder*')) current @endif"><a href="{{url('myorder')}}">My Orders</a></li>
    {{-- <li class="@if(Request::is('billing')) current @endif"><a href="{{url('billing')}}">Billing Agreements</a></li> --}}
    <li class="@if(Request::is('tracking')) current @endif"><a href="{{url('tracking')}}">Check Tracking Shipping</a></li>
    <li class="last"><a href="{{url('logout')}}">Logout</a></li>
</ul>