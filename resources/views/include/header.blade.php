  
  
  <!-- Header -->
  <header id="header">
    <div class="header-container">
      <div class="header-top">
        <div class="container">
          <div class="row"> 
            <!-- top links -->
            <div class="headerlinkmenu col-md-4 col-sm-7 col-xs-6"> 
              <!-- Default Welcome Message -->
              <div class="welcome-msg hidden-xs">Welcome to {{$set->nama_toko}}! </div>
              <div class="links">
                <div class="jtv-user-info">

                  @if(Auth::check())
                  <div class="dropdown"> 
                    <a class="current-open" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#"><span>My Account </span> <i class="fa fa-angle-down"></i></a>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="{{url('memberarea')}}">Account</a></li>
                      <li><a href="{{url('myorder')}}">My Orders</a></li>
                      {{-- <li><a href="{{url('memberarea')}}">Billing Agreements</a></li> --}}
                      <li class="divider"></li>
                      <li><a href="{{url('logout')}}">Logout</a></li>
                    </ul>
                  </div>
                  @else
                  <a href="{{url('login')}}">Login</a><a>/</a><a href="{{url('register')}}">Register</a>
                  @endif
                </div>
              </div>
            </div>
            <div class="col-md-5 col-sm-5 col-xs-6 hidden-xs hidden-sm">
              <div class="slider-items-products">
                <div id="offer-slider" class="product-flexslider hidden-buttons">
                  <div class="slider-items slider-width-col4">
                    <div class="offer-content-text">
                      <p><i class="fa fa-truck"></i> FREE SHIPPING FOR ORDERS OVER <span>$99</span></p>
                    </div>
                    <div class="offer-content-text">
                      <p><i class="fa fa-thumbs-up"></i> Only the <span>best quality</span> and brands</p>
                    </div>
                    <div class="offer-content-text">
                      <p><i class="fa fa-gift"></i> Free gift after every <span>10 order</span></p>
                    </div>
                    <div class="offer-content-text">
                      <p><i class="fa fa-repeat"></i> 100% money back <span>guarantee</span>.</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {{-- <div class="col-md-3 col-sm-5 col-xs-6">
              <div class="language-currency-wrapper pull-right">
                <div class="inner-cl">
                  <div class="block block-language form-language">
                    <div class="lg-cur"> <span> <span class="lg-fr">English</span> <i class="fa fa-angle-down"></i> </span> </div>
                    <ul>
                      <li> <a href="#"><span>Indonesia</span> </a> </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div> --}}
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row"> 
          
          <!-- Start Menu Area -->
          <div class="menu-area">
            <div class="container">
              <div class="row">
                <div class="col-md-12">
                  <div class="main-menu">
                    <nav> <!-- Header Logo -->
                      <div class="logo"><a href="{{url('/')}}"><h2>{{$set->nama_toko}}</h2></a></div>
                      <!-- End Header Logo -->
                      <ul class="hidden-xs">
                        {{-- <li class="active"><a href="{{url('/')}}">Home</a></li> --}}
                        @foreach($kat as $dat_kat)
                          @if(\App\SubKategori::where('id_kategori',$dat_kat->id)->count() > 0)
                            <li class="megamenu"><a href="{{url('product/cat/'.$dat_kat->slug)}}">{{$dat_kat->name}}</a>
                              <div class="mega-menu">
                                <div class="menu-block menu-block-center">
                                  <div class="menu-block-1">
                                    @foreach(\App\SubKategori::where('id_kategori',$dat_kat->id)->get() as $dat_sub)
                                    <ul>
                                      <li> <a href="{{url('product/'.$dat_kat->slug.'/'.$dat_sub->slug)}}">{{$dat_sub->name}}</a></li>
                                    </ul>
                                    @endforeach
                                  </div>
                                </div>
                              </div>
                            </li>
                          @else
                          <li><a href="{{url('product/cat/'.$dat_kat->slug)}}">{{$dat_kat->name}}</a></li>
                          @endif
                        @endforeach
                      </ul>
                      <!-- Start Mobile Menu -->
                      <div class="mobile-menu hidden-sm hidden-md hidden-lg">
                        <nav>
                          <ul>
                            @foreach($kat as $dat_kat)
                            <li class=""><a href="{{url('product/cat/'.$dat_kat->slug)}}">{{$dat_kat->name}}</a>
                              <ul class="">
                                  @foreach(\App\SubKategori::where('id_kategori',$dat_kat->id)->get() as $dat_sub)
                                  <li><a href="{{url('product/'.$dat_kat->slug.'/'.$dat_sub->slug)}}">{{$dat_sub->name}}</a></li>
                                  @endforeach
                              </ul>
                            </li>
                            @endforeach
                          </ul>
                        </nav>
                      </div>
                    </nav>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
        </div>
      </div>
    </div>
    <div class="header-bottom">
      <div class="container">
        <div class="row">
          <div class="col-xs-10 col-sm-8"> 
            <!-- Search -->
            
            <div class="top-search">
              <div id="search">
                <form>
                  <div class="input-group">
                    {{-- <select class="cate-dropdown hidden-xs hidden-sm" name="category_id">
                      <option>All Categories</option>
                      @foreach($kat as $dat_kat)
                      <option value="{{$dat_kat->id}}">{{$dat_kat->name}}</option>
                        @if(\App\SubKategori::where('id_kategori',$dat_kat->id)->count() > 0)
                          @foreach(\App\SubKategori::where('id_kategori',$dat_kat->id)->get() as $dat_sub)
                            <option value="{{$dat_sub->id}}">&nbsp;&nbsp;&nbsp;{{$dat_sub->name}}</option>
                          @endforeach
                        @endif
                      @endforeach
                    </select> --}}
                    <input type="text" class="form-control" placeholder="Search" name="search">
                    <button class="btn-search" type="button"><i class="fa fa-search"></i></button>
                  </div>
                </form>
              </div>
            </div>
            
            <!-- End Search --> 
          </div>
          <div class="col-lg-4 col-sm-4 col-xs-2 top-cart"> <span class="phone hidden-xs hidden-sm"><i class="fa fa-phone fa-lg"></i> {{$set->phone}}</span> 
            @if(Auth::check())
            <!-- Begin shopping cart trigger  -->
            <div id="shopping-cart-trigger">
              <a href="{{url('cart')}}" class="cart-icon"> 
                <i class="fa fa-shopping-bag"></i> 
                @if((Auth::check()) && (\App\Cart::where('id_user',Auth::user()->id)->count() > 0))
                  <span class="cart-num">{{\App\Cart::where('id_user',Auth::user()->id)->count()}}</span> 
                @endif
                <span class="top-cart-label hidden-xs">My Bag</span>
              </a> 
           </div>
            <!-- End shopping cart trigger --> 
            @endif
            
          </div>
        </div>
      </div>
    </div>
  </header>
  <!-- end header --> 