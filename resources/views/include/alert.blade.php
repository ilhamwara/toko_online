<script src="{{asset('js/sweetalert.js')}}"></script>
<script>
    @if(Session::has('success'))
        swal("Berhasil","{{Session::get('success')}}","success");
    @elseif(Session::has('error'))
        swal("Gagal","{{Session::get('error')}}","error");
    @elseif(Session::has('warning'))
        swal("Warning","{{Session::get('warning')}}","warning");
    @endif
</script>