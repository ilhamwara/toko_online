<style>
  table{width: 100%!important;}
</style>
<div>
    <img src="https://indowebremote.com/public/images/logo.png" alt="" width="100" border="0">
    <br>
    <br>

    @php
        $trans  = \App\Transaksi::where('kode_order',$kode)->first();
        $set    = \App\Setting::findOrFail(1);
        $pro    = \App\TransaksiProduk::where('id_transaksi',$trans->id)->get();
    @endphp
    
    <h4>Hi {{$nama}},</h4>
    <p>{{$text}}</p>
    <table class="table table-striped table-bordered" style="width: 100%;">
      <tr>
        <th colspan="3" class="text-center" style="background:#34495e; color:#fff; padding: 10px;">SHIPPING</th>
      </tr>
      <tr>
        <td width="25%">Order Code</td>
        <td class="text-center"  width="5%">:</td>
        <td>{{$trans->kode_order}}</td>
      </tr>
      <tr>
        <td width="25%">Name</td>
        <td class="text-center"  width="5%">:</td>
        <td>{{$trans->nama}}</td>
      </tr>
      <tr>
        <td width="25%">Email</td>
        <td class="text-center"  width="5%">:</td>
        <td>{{$trans->email}}</td>
      </tr>
      <tr>
        <td width="25%">Phone</td>
        <td class="text-center"  width="5%">:</td>
        <td>{{$trans->telp}}</td>
      </tr>
      <tr>
        <td width="25%">Province</td>
        <td class="text-center"  width="5%">:</td>
        <td>{{$trans->provinsi}}</td>
      </tr>
      <tr>
        <td>City</td>
        <td class="text-center" >:</td>
        <td>{{$trans->kota}}</td>
      </tr>
      <tr>
        <td>Sub District</td>
        <td class="text-center" >:</td>
        <td>{{$trans->kecamatan}}</td>
      </tr>
      <tr>
        <td>Full Address</td>
        <td class="text-center" >:</td>
        <td>{{$trans->alamat}}</td>
      </tr>
      <tr>
        <td>Courier</td>
        <td class="text-center" >:</td>
        <td>{{$trans->kurir}}</td>
      </tr>
      <tr>
        <td>Shipping Type</td>
        <td class="text-center" >:</td>
        <td>{{$trans->shipping_desc}}</td>
      </tr>
      <tr>
        <td>Shipping Estimate</td>
        <td class="text-center" >:</td>
        <td>{{$trans->etd}}</td>
      </tr>
    </table>
    <br><br>
    <table class="table table-striped table-bordered" style="width: 100%;">
      <tr>
        <th colspan="4" class="text-center" style="padding: 10px;background:#34495e; color:#fff;">PRODUCTS</th>
      </tr>
      @foreach($pro as $data)
      <tr>
        <td>{{\App\Produk::where('id',$data->id_produk)->value('name')}}</td>
        <td>{{$data->qty}} Unit</td>
        <td width="20%">Rp {{str_replace(',','.',number_format($data->total))}}</td>
      </tr>
      @endforeach
      <tr>
        <td colspan="3">Sub Total</td>
        <td>Rp {{str_replace(',','.',number_format($pro->sum('total')))}}</td>
      </tr>
      <tr>
        <td colspan="3">Shipping</td>
        <td>Rp {{str_replace(',','.',number_format($trans->shipping_price))}}</td>
      </tr>
      <tr>
        <td colspan="3">Grand Total</td>
        <td>Rp {{str_replace(',','.',number_format($pro->sum('total')+$trans->shipping_price))}}</td>
      </tr>
    </table>
    <br><br>
    <div style="font-family:Arial,Helvetica,sans-serif;font-size:13px;color:#000;line-height:18px;padding:5px 0 0 0;margin:0;text-align: center;">
        Jika ada pertanyaan, silahkan hubungi Customer Service kami :
        <br>Telp. {{$set->phone}}
        <br>Email. {{$set->email}}</div>
    <br>
    <div style="text-align: center;">THANK YOU FOR SHOPPING AT {{$set->nama_toko}}</div>
</div>