<?php 
namespace App\Library;

class RajaOngkirLib
{
    public static function provinsi()
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt_array($curl, array(
          CURLOPT_URL               => "https://pro.rajaongkir.com/api/province",
          CURLOPT_RETURNTRANSFER    => true,
          CURLOPT_ENCODING          => "",
          CURLOPT_MAXREDIRS         => 10,
          CURLOPT_TIMEOUT           => 30,
          CURLOPT_HTTP_VERSION      => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST     => "GET",
          CURLOPT_HTTPHEADER        => array(
            "key: 7c43282331035323186e9398338d5328"
          ),
        ));

        $response = curl_exec($curl);
        $err       = curl_error($curl);
        curl_close($curl);

        if ($err) {
          return "cURL Error #:" . $err;
        } else {
          return json_decode($response);
        }
    }
    public static function kota($id_provinsi)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt_array($curl, array(
          CURLOPT_URL               => "https://pro.rajaongkir.com/api/city?province=".$id_provinsi,
          CURLOPT_RETURNTRANSFER    => true,
          CURLOPT_ENCODING          => "",
          CURLOPT_MAXREDIRS         => 10,
          CURLOPT_TIMEOUT           => 30,
          CURLOPT_HTTP_VERSION      => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST     => "GET",
          CURLOPT_HTTPHEADER        => array(
            "key: 7c43282331035323186e9398338d5328"
          ),
        ));

        $response = curl_exec($curl);
        $err       = curl_error($curl);
        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
          return json_decode($response);
        }
    }
    public static function subdistrict($id_city)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt_array($curl, array(
          CURLOPT_URL               => "https://pro.rajaongkir.com/api/subdistrict?city=".$id_city,
          CURLOPT_RETURNTRANSFER    => true,
          CURLOPT_ENCODING          => "",
          CURLOPT_MAXREDIRS         => 10,
          CURLOPT_TIMEOUT           => 30,
          CURLOPT_HTTP_VERSION      => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST     => "GET",
          CURLOPT_HTTPHEADER        => array(
            "key: 7c43282331035323186e9398338d5328"
          ),
        ));

        $response = curl_exec($curl);
        $err       = curl_error($curl);
        curl_close($curl);

        if ($err) {
          return "cURL Error #:" . $err;
        } else {
          return json_decode($response);
        }
    }
    public static function cost($kota_asal,$kota_tujuan,$berat,$courier)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://pro.rajaongkir.com/api/cost",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "origin=".$kota_asal."&originType=subdistrict&destination=".$kota_tujuan."&destinationType=subdistrict&weight=".$berat."&courier=".$courier,
          CURLOPT_HTTPHEADER => array(
            "content-type: application/x-www-form-urlencoded",
            "key: 7c43282331035323186e9398338d5328"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          return "cURL Error #:" . $err;
        } else {
          return json_decode($response);
        }
    }
}

