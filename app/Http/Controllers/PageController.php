<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use App\Menu;

class PageController extends Controller
{
    public function index()
    {
        $title = 'Page';
        $page = Page::orderBy('id','DESC')->get();
        return view('admin.page.index',compact('title','page'));
    }
    public function tambah()
    {
        $title  = 'Add Page';
        $menu   = Menu::all();
        return view('admin.page.tambah',compact('title','menu'));
    }
    public function edit($id)
    {
        $title  = 'Edit Page';
        $page   = Page::findOrFail($id);
        $menu   = Menu::all();
        return view('admin.page.edit',compact('title','page','menu'));
    }
    public function post_tambah(Request $r)
    {
        if (Page::where('menu',$r->menu)->count() > 0) {
            return redirect('administrator/page/add')->with('warning','Oppss menu has been added');
        }
        
        $validate = $r->validate([
            'description'   => 'required',
            'menu'          => 'required|integer',
        ]);

        $page = new Page;
        $page->description  = $r->description;
        $page->menu         = $r->menu;
        $page->save();

        return redirect('administrator/page/add')->with('success','Success save data');
    }
    public function post_edit(Request $r,$id)
    {
        if (Page::where('menu',$r->menu)->where('id','!=',$id)->count() > 0) {
            return redirect('administrator/page/add')->with('warning','Oppss menu has been added');
        }
        $validate = $r->validate([
            'description'   => 'required',
            'menu'          => 'required|integer',
        ]);

        $page = Page::findOrFail($id);
        $page->description  = $r->description;
        $page->menu         = $r->menu;
        $page->save();

        return redirect('administrator/page/edit/'.$id)->with('success','Success edit data');
    }
    public function delete($id)
    {
        Page::where('id',$id)->delete();
        return redirect('administrator/page')->with('success','Success delete data');
    }
}
