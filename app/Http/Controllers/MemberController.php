<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManager;
use App\User;
use App\Setting;
use App\Kategori;
use App\Menu;
use App\Transaksi;
use App\TransaksiProduk;
use App\FotoProduk;
use App\Cart;
use App\Produk;
use App\Payment;
use Hash;
use Auth;
use Mail;
use App\Library\RajaOngkirLib;

class MemberController extends Controller
{
    public function memberarea()
    {
        $menu_info    = Menu::where('title','Information')->get();
        $menu_insider = Menu::where('title','Insider')->get();
        $menu_service = Menu::where('title','Service')->get();
        $set          = Setting::findOrFail(1);
        $kat          = Kategori::all();

        return view('front.memberarea',compact('set','kat','menu_info','menu_insider','menu_service'));
    }
    public function address_book()
    {
        $menu_info    = Menu::where('title','Information')->get();
        $menu_insider = Menu::where('title','Insider')->get();
        $menu_service = Menu::where('title','Service')->get();
        $set          = Setting::findOrFail(1);
        $kat          = Kategori::all();
        
        return view('front.address_book',compact('set','kat','menu_info','menu_insider','menu_service'));
    }
    public function myorder()
    {
        $menu_info    = Menu::where('title','Information')->get();
        $menu_insider = Menu::where('title','Insider')->get();
        $menu_service = Menu::where('title','Service')->get();
        $set          = Setting::findOrFail(1);
        $kat          = Kategori::all();
        $trans        = Transaksi::orderBy('id','DESC')->where('id_user',Auth::user()->id)->get();
        
        return view('front.myorder',compact('set','kat','menu_info','menu_insider','menu_service','trans'));
    }
    public function billing()
    {
        $menu_info    = Menu::where('title','Information')->get();
        $menu_insider = Menu::where('title','Insider')->get();
        $menu_service = Menu::where('title','Service')->get();
        $set          = Setting::findOrFail(1);
        $kat          = Kategori::all();
        
        return view('front.billing',compact('set','kat','menu_info','menu_insider','menu_service'));
    }
    public function tracking()
    {
        $menu_info    = Menu::where('title','Information')->get();
        $menu_insider = Menu::where('title','Insider')->get();
        $menu_service = Menu::where('title','Service')->get();
        $set          = Setting::findOrFail(1);
        $kat          = Kategori::all();
        
        return view('front.tracking',compact('set','kat','menu_info','menu_insider','menu_service'));
    }
    public function profile(Request $r)
    {
        $validate = $r->validate([
            'name'      => 'required|string|max:255',
            'email'     => 'required|string|email',
            'alamat'    => 'required|string|max:255',
            'kota'      => 'required|string|max:255',
            'phone'     => 'required|string|max:255',
        ]);

        $user = User::findOrFail(Auth::user()->id);
        $user->name      = $r->name;
        $user->email     = $r->email;
        $user->alamat    = $r->alamat;
        $user->kota      = $r->kota;
        $user->phone     = $r->phone;
        if (!empty($r->password)) {
            $user->password  = Hash::make($r->password);
        }
        $user->save();

        return redirect('memberarea')->with('success','Berhasil update profile');

    }
    public function checkout(Request $r)
    {
        $validate = $r->validate([
            'province'       => 'required',
            'city'           => 'required',
            'sub_district'   => 'required',
            'courier'        => 'required',
            'shipping_type'  => 'required',
            'address'        => 'required',
            'name'           => 'required',
            'email'          => 'required',
            'phone'          => 'required',

        ]);
        // Express Sameday Barang
        $cart    = Cart::where('id_user',Auth::user()->id)->get();
        $cost    = RajaOngkirLib::cost(2119,$r->id_kec,1000,$r->courier);
        if (!empty($cost)) {
            foreach ($cost->rajaongkir->results[0]->costs as $k => $v) {
                if ($v->service == $r->shipping_type) {
                    $trans = new Transaksi;
                    $trans->id_user          = Auth::user()->id;
                    $trans->kode_order       = date('d').str_random(3).date('m').str_random(3).date('Y').str_random(2);
                    $trans->nama             = $r->name;
                    $trans->email            = $r->email;
                    $trans->telp             = $r->phone;
                    $trans->provinsi         = $r->province;
                    $trans->kota             = $r->city;
                    $trans->kecamatan        = $r->sub_district;
                    $trans->alamat           = $r->address;
                    $trans->kurir            = $cost->rajaongkir->results[0]->name;
                    $trans->shipping_type    = $r->shipping_type;
                    $trans->shipping_desc    = $v->description;
                    $trans->shipping_price   = $v->cost[0]->value;
                    $trans->etd              = $v->cost[0]->etd;
                    $trans->status           = 0;
                    $trans->save();
                }
            }
            foreach ($cart as $key => $value) {
                $trans_prod = new TransaksiProduk;
                $trans_prod->id_user        = Auth::user()->id;
                $trans_prod->id_transaksi   = $trans->id;
                $trans_prod->id_produk      = $value->id_produk;
                $trans_prod->qty            = $value->qty;
                $trans_prod->diskon         = (Produk::where('id',$value->id_produk)->value('diskon') > 0 ? Produk::where('id',$value->id_produk)->value('diskon') : 0);
                $trans_prod->harga          = Produk::where('id',$value->id_produk)->value('harga');
                $trans_prod->total          = $value->total;
                $trans_prod->save();
            }
            Cart::where('id_user',Auth::user()->id)->delete();
        }else{
            return redirect('cart')->with('warning','OOPPSS You Having Trouble System Please Try Again');
        }

        return redirect('myorder/'.$trans->id)->with('success','Your Order Has Been Success');
    }
    public function detail_order($id)
    {
        $menu_info    = Menu::where('title','Information')->get();
        $menu_insider = Menu::where('title','Insider')->get();
        $menu_service = Menu::where('title','Service')->get();
        $set          = Setting::findOrFail(1);
        $kat          = Kategori::all();
        $trans        = Transaksi::where('id',$id)->where('id_user',Auth::user()->id)->first();
        $pro          = TransaksiProduk::where('id_transaksi',$id)->where('id_user',Auth::user()->id)->get();
        
        return view('front.detail_order',compact('set','kat','menu_info','menu_insider','menu_service','trans','pro'));
    }
    public function post_tracking(Request $r)
    {
        $validate = $r->validate([
            'order_code'       => 'required',

        ]);

        $trans = Transaksi::where('kode_order',$r->order_code)->first();
        $pro = TransaksiProduk::where('id_transaksi',$trans->id)->get();
        $menu_info    = Menu::where('title','Information')->get();
        $menu_insider = Menu::where('title','Insider')->get();
        $menu_service = Menu::where('title','Service')->get();
        $set          = Setting::findOrFail(1);
        $kat          = Kategori::all();

        return view('front.tracking',compact('trans','pro','menu_info','menu_insider','menu_service','set','kat'));
    }
    public function payment(Request $r,$kode)
    {
        $validate = $r->validate([
            'account_name'      => 'required',
            'bank_name'         => 'required',
            'bank_receipt'      => 'required',
        ]);

        if (!empty($r->bank_receipt)) {
            $extfile    = $r->file('bank_receipt')->getClientOriginalExtension();
            if(($extfile == 'jpg') || ($extfile == 'jpeg') || ($extfile == 'png') ){
                if ($r->hasFile('bank_receipt')) {
                    $gambar = date('d-m-y').'-'.str_random(10).'.'.$r->file('bank_receipt')->getClientOriginalExtension();
                    $manager      = new ImageManager();
                    $fit          = $manager->make($r->bank_receipt)->save(public_path().'/images/bukti/'.$gambar);

                    $trans = Transaksi::where('kode_order',$kode)->first();

                    $pay = new Payment;
                    $pay->id_user       = Auth::user()->id;
                    $pay->id_transaksi  = $trans->id;
                    $pay->nama          = $r->account_name;
                    $pay->bank          = $r->bank_name;
                    $pay->bukti         = $gambar;
                    $pay->status        = 0;
                    $pay->save();

                    $trans->status = 1;
                    $trans->save();

                    $data_user = [
                            'kode'       => $trans->kode_order,
                            'email'      => $trans->email,
                            'nama'       => $trans->nama,
                            'telp'       => $trans->telp,
                            'img'        => FotoProduk::where('id_produk',TransaksiProduk::where('id_transaksi',$trans->id)->value('id_produk'))->value('img_full'),
                            'text'       => 'Terimakasih sudah melakukan pembayaran order anda tunggu konfirmasi pembayaran dari kami, berikut detail order anda',
                        ];
                    Mail::send('mail.payment_confirm', $data_user, function ($message) use ($data_user) {
                        $message->from('indowebremote@gmail',Setting::where('id',1)->value('nama_toko'));
                        $message->to($data_user['email']);
                        $message->subject('Payment Has Been Sent');
                    });

                    return redirect('myorder')->with('success', 'Your data has been sent wait until admin confirm');
                }           
            }
            else{
                return redirect()->back()->with('error', 'Format must be jpg , jpeg or png');
            }
        }
    }
}