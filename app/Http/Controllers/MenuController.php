<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;
use App\Kategori;

class MenuController extends Controller
{
    public function index()
    {
        $title = 'Menu';
        $menu = Menu::orderBy('id','DESC')->get();
        return view('admin.menu.index',compact('menu','title'));
    }
    public function tambah()
    {
        $title = 'Add Menu';
        $kat = Kategori::all();
        return view('admin.menu.tambah',compact('kat','title'));
    }
    public function edit($id)
    {
        $title = 'Edit Menu';
        $menu = Menu::findOrFail($id);
        $kat = Kategori::all();
        return view('admin.menu.edit',compact('menu','kat','title'));
    }
    public function post_tambah(Request $r)
    {
        if (Menu::where('nama',$r->nama)->count() > 0) {
            return redirect('administrator/menu/add')->with('warning','Ooppss data has been added');
        }

        $validate = $r->validate([
            'nama'          => 'required',
            'link'          => 'required',
            'title'         => 'required',
        ]);

        $menu = new Menu;
        $menu->title        = $r->title;
        $menu->nama         = $r->nama;
        $menu->type         = 'footer';
        $menu->link         = $r->link;
        $menu->aktif        = 1;
        $menu->save();

        return redirect('administrator/menu/add')->with('success','Success save data');
    }
    public function post_edit(Request $r,$id)
    {
        if (Menu::where('nama',$r->nama)->where('id','!=',$id)->count() > 0) {
            return redirect('administrator/menu/edit/'.$id)->with('warning','Ooppss data has been added');
        }

        $validate = $r->validate([
            'nama'          => 'required',
            'link'          => 'required',
            'title'         => 'required',
        ]);

        $menu = Menu::findOrFail($id);
        $menu->title        = $r->title;
        $menu->nama         = $r->nama;
        $menu->link         = $r->link;
        $menu->aktif        = 1;
        $menu->save();

        return redirect('administrator/menu/edit/'.$id)->with('success','Success edit data');
    }
    public function delete($id)
    {
        Menu::where('id',$id)->delete();
        return redirect('administrator/menu')->with('success','Success delete data');
    }
}
