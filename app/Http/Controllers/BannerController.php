<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Banner;
use App\Kategori;
use Intervention\Image\ImageManager;

class BannerController extends Controller
{
    public function index()
    {
        $title = 'Banner';
        $banner = Banner::all();
        return view('admin.banner.index',compact('title','banner'));
    }
    public function tambah()
    {
        $title = 'Add Banner';
        $kat = Kategori::all();
        return view('admin.banner.tambah',compact('title','kat'));
    }
    public function edit($id)
    {
        $title = 'Edit Banner';
        $banner = Banner::findOrFail($id);

        return view('admin.banner.edit',compact('title','banner'));
    }
    public function post_tambah(Request $r)
    {
        $validate = $r->validate([
            'img'  => 'required',
            'type' => 'required',
            'urutan' => 'required',
        ]);

        $banner = new Banner;
        if (!empty($r->id_kategori)) {
            $banner->id_kategori =   $r->id_kategori;
        }
        if (!empty($r->img)) {
            $extfile    = $r->file('img')->getClientOriginalExtension();
            if(($extfile == 'svg') || ($extfile == 'jpg') || ($extfile == 'jpeg') || ($extfile == 'png') ){
                if ($r->hasFile('img') ) {
                    $gambar = date('d-m-y').'-'.str_random(10).'.'.$r->file('img')->getClientOriginalExtension();

                    $manager      = new ImageManager();
                    $fit          = $manager->make($r->img)->save(public_path().'/images/slider/'.$gambar);

                    $banner->img         =   $gambar;
                }           
            }
            else{
                return redirect()->back()->with('error', 'Format must be svg , jpg , jpeg or png');
            }
        }
        
        $banner->type        =   $r->type;
        $banner->link        =   $r->link;
        $banner->urutan      =   $r->urutan;
        $banner->save();


        return redirect('administrator/banner/add')->with('success','Success save data');
    }
    public function post_edit(Request $r,$id)
    {
        $validate = $r->validate([
            'type' => 'required',
            'urutan' => 'required',
        ]);

        $banner = Banner::findOrFail($id);
        if (!empty($r->id_kategori)) {
            $banner->id_kategori =   $r->id_kategori;
        }
        if (!empty($r->img)) {
            $extfile    = $r->file('img')->getClientOriginalExtension();
            if(($extfile == 'svg') || ($extfile == 'jpg') || ($extfile == 'jpeg') || ($extfile == 'png') ){
                if ($r->hasFile('img') ) {
                    $gambar = date('d-m-y').'-'.str_random(10).'.'.$r->file('img')->getClientOriginalExtension();

                    $manager      = new ImageManager();
                    $fit          = $manager->make($r->img)->save(public_path().'/images/slider/'.$gambar);

                    $banner->img  =   $gambar;
                }           
            }
            else{
                return redirect()->back()->with('error', 'Format must be svg , jpg , jpeg or png');
            }
        }
        
        $banner->type        =   $r->type;
        $banner->link        =   $r->link;
        $banner->urutan      =   $r->urutan;
        $banner->save();


        return redirect('administrator/banner/edit/'.$id)->with('success','Success edit data');
    }
    public function delete($id)
    {
        Banner::where('id',$id)->delete();
        return redirect('administrator/banner')->with('success','Success delete data');
    }
}
