<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\Setting;
use App\Transaksi;
use App\TransaksiProduk;
use Hash;
use Auth;
use DB;

class AdminController extends Controller
{
    public function login()
    {
        if (Auth::check()) {
            return redirect()->back();
        }
        return view('admin.login');
    }
    public function postlogin(Request $r)
    {
        if (Auth::check()) {
            return redirect()->back();
        }
        $validate = $r->validate([
            'email'     => 'required|string|email',
            'password'  => 'required',
        ]);

        if(Auth::attempt(['email' => $r->email, 'password' => $r->password,'type' => 'admin'])) {
            return redirect()->intended('administrator/dashboard')->with('success','Anda berhasil login');
        }else{
            return redirect()->back()->with('error','Ooppss terdapat kesalahan login');
        }
    }
    public function dashboard()
    {
        $trans          = Transaksi::orderBy('id','DESC')->limit(10)->get();
        $pro            = TransaksiProduk::join('produk','transaksi_produk.id_produk','=','produk.id')->select(DB::Raw('produk.id,produk.name, COUNT(*) as count'),'transaksi_produk.id_produk')->groupBy('transaksi_produk.id_produk')->orderBy('count','DESC')->limit(10)->get();
        $user           = User::where('type','member')->count();
        $order_pending  = Transaksi::where('status',1)->count();
        $order_today    = Transaksi::whereDay('created_at',date('d'))->count();
        $earning        = TransaksiProduk::join('transaksi','transaksi_produk.id_transaksi','=','transaksi.id')->whereIn('transaksi.status',[2,3])->whereDay('transaksi.created_at',date('d'))->sum('total');

        return view('admin.dashboard',compact('trans','pro','user','order_pending','order_today','earning'));
    }
    public function setting()
    {
        $title = 'Setting Site';
        $set = Setting::findOrFail(1);
        return view('admin.setting',compact('title','set'));
    }
    public function post_setting(Request $r)
    {
        $validate = $r->validate([
            'nama_toko'         => 'required|string',
            'email'             => 'required|string|email',
            'phone'             => 'required',
            'alamat'            => 'required',
        ]);

        $set = Setting::findOrFail(1);
        $set->nama_toko         = $r->nama_toko;
        $set->email             = $r->email;
        if (!empty($r->password_email)) {
            $set->password_email    = $r->password_email;
        }
        $set->phone             = $r->phone;
        $set->alamat            = $r->alamat;
        $set->facebook          = $r->facebook;
        $set->twitter           = $r->twitter;
        $set->instagram         = $r->instagram;
        $set->copyright         = $r->copyright;
        $set->google_analytics  = $r->google_analytics;
        $set->google_adwords    = $r->google_adwords;
        $set->live_chat         = $r->live_chat;
        $set->save();

        return redirect('administrator/setting_site')->with('success','Data has been updated');
    }
}
