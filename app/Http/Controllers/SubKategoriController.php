<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kategori;
use App\SubKategori;

class SubKategoriController extends Controller
{
    public function index()
    {
        $title = 'Sub Category';
        $sub = SubKategori::all();
        return view('admin.subkategori.index',compact('title','sub'));
    }
    public function tambah()
    {
        $title = 'Add Sub Category';
        $kat = Kategori::all();
        return view('admin.subkategori.tambah',compact('title','kat'));
    }
    public function edit($id)
    {
        $title = 'Edit Sub Category';
        $sub = SubKategori::findOrFail($id);
        $kat = Kategori::all();

        return view('admin.subkategori.edit',compact('title','kat','sub'));
    }
    public function post_tambah(Request $r)
    {
        $validate = $r->validate([
            'id_kategori'  => 'required',
            'name'         => 'required',
        ]);

        if (SubKategori::where('id_kategori',$r->id_kategori)->where('name',$r->name)->count() > 0) {
            return redirect('administrator/categories/sub/add')->with('warning','Ooppss sorry name cannot be same');            
        }

        $kat = new SubKategori;
        $kat->id_kategori = $r->id_kategori;
        $kat->name        = $r->name;
        $kat->slug        = str_slug($r->name);
        $kat->save();

        return redirect('administrator/categories/sub/add')->with('success','Success save data');
    }
    public function post_edit(Request $r,$id)
    {
        $validate = $r->validate([
            'id_kategori' => 'required',
            'name'        => 'required',
        ]);

        if (SubKategori::where('id','!=',$id)->where('id_kategori',$r->id_kategori)->where('name',$r->name)->count() > 0) {
            return redirect('administrator/categories/sub/add')->with('warning','Ooppss sorry name cannot be same');            
        }

        $kat = SubKategori::findOrFail($id);
        $kat->id_kategori = $r->id_kategori;
        $kat->name        = $r->name;
        $kat->slug        = str_slug($r->name);
        $kat->save();


        return redirect('administrator/categories/sub/edit/'.$id)->with('success','Success edit data');
    }
    public function delete($id)
    {
        SubKategori::where('id',$id)->delete();
        return redirect('administrator/categories/sub')->with('success','Success delete data');
    }
}
