<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Artikel;
use Auth;

class ArtikelController extends Controller
{
    public function index()
    {
        $title = 'Article';
        $artikel = Artikel::all();
        return view('admin.artikel.index',compact('title','artikel'));
    }
    public function tambah()
    {
        $title = 'Add Article';

        return view('admin.artikel.tambah',compact('title'));
    }
    public function edit($id)
    {
        $title = 'Edit Article';
        $artikel = Artikel::findOrFail($id);

        return view('admin.artikel.edit',compact('title','artikel'));
    }
    public function post_tambah(Request $r)
    {
        $validate = $r->validate([
            'title'       => 'required',
            'description' => 'required',
        ]);

        $artikel = new Artikel;
        $artikel->title       = $r->title;
        $artikel->description = $r->description;
        $artikel->id_user     = Auth::user()->id;
        $artikel->save();

        return redirect('administrator/artikel/add')->with('success','Success save data');
    }
    public function post_edit(Request $r,$id)
    {
        $validate = $r->validate([
            'title'       => 'required',
            'description' => 'required',
        ]);

        $artikel = Artikel::findOrFail($id);
        $artikel->title       = $r->title;
        $artikel->description = $r->description;
        $artikel->save();

        return redirect('administrator/artikel/edit/'.$id)->with('success','Success edit data');
    }
    public function delete($id)
    {
        Artikel::where('id',$id)->delete();
        return redirect('administrator/artikel')->with('success','Success delete data');
    }
}
