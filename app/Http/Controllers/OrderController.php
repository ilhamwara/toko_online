<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaksi;
use App\TransaksiProduk;
use App\FotoProduk;
use App\Payment;
use App\Setting;
use User;
use Mail;

class OrderController extends Controller
{
    public function index()
    {
        $title = 'All Orders';
        $trans = Transaksi::orderBy('id','DESC')->get();

        return view('admin.order.index',compact('trans','title'));
    }
    public function pending()
    {
        $title = 'Pending Orders';
        $trans = Transaksi::where('status',1)->orderBy('id','DESC')->get();
        return view('admin.order.index',compact('trans','title'));
    }
    public function confirm()
    {
        $title = 'Confirms Orders';
        $trans = Transaksi::where('status',2)->orderBy('id','DESC')->get();
        return view('admin.order.index',compact('trans','title'));
    }
    public function confirm_shipping()
    {
        $title = 'Confirm Shipping';
        $trans = Transaksi::where('status',3)->orderBy('id','DESC')->get();
        return view('admin.order.index',compact('trans','title'));
    }
    public function rejected()
    {
        $title = 'Rejected Orders';
        $trans = Transaksi::where('status',4)->orderBy('id','DESC')->get();
        return view('admin.order.index',compact('trans','title'));
    }
    public function detail($id)
    {
        $title = 'Detail Orders';

        $trans  = Transaksi::findOrfail($id);
        $pro    = TransaksiProduk::where('id_transaksi',$id)->get();
        $bukti  = Payment::where('id_transaksi',$id)->get();

        return view('admin.order.detail',compact('trans','title','pro','bukti'));
    }
    public function approve($id)
    {
        $trans  = Transaksi::findOrfail($id);
        $trans->status = 2;
        $trans->save();

        $data_user = [
                        'kode'       => $trans->kode_order,
                        'email'      => $trans->email,
                        'nama'       => $trans->nama,
                        'telp'       => $trans->telp,
                        'img'        => FotoProduk::where('id_produk',TransaksiProduk::where('id_transaksi',$id)->value('id_produk'))->value('img_full'),
                        'text'       => 'Pembayaran anda sudah kami konfirmasi, Detail Order Sebagai Berikut,',
                    ];
                Mail::send('mail.payment_confirm', $data_user, function ($message) use ($data_user) {
                    $message->from('indowebremote@gmail',Setting::where('id',1)->value('nama_toko'));
                    $message->to($data_user['email']);
                    $message->subject('Payment Has Confirmed');
                });

        return redirect()->back()->with('success','Berhasil update data');
    }
    public function tolak($id)
    {
        $trans  = Transaksi::findOrfail($id);
        $trans->status = 4;
        $trans->save();

        $data_user = [
                        'kode'       => $trans->kode_order,
                        'email'      => $trans->email,
                        'nama'       => $trans->nama,
                        'telp'       => $trans->telp,
                        'img'        => FotoProduk::where('id_produk',TransaksiProduk::where('id_transaksi',$id)->value('id_produk'))->value('img_full'),
                        'text'       => 'Mohon Maaf Pembayaran anda kami tolak detail order sebagai berikut,',
                    ];
                Mail::send('mail.payment_confirm', $data_user, function ($message) use ($data_user) {
                    $message->from('indowebremote@gmail',Setting::where('id',1)->value('nama_toko'));
                    $message->to($data_user['email']);
                    $message->subject('Payment Has Rejected');
                });

        return redirect()->back()->with('success','Berhasil update data');
    }
    public function shipping($id)
    {
        $trans  = Transaksi::findOrfail($id);
        $trans->status = 3;
        $trans->save();

        return redirect()->back()->with('success','Berhasil update data');
    }
    public function resi(Request $r,$id)
    {
        $trans  = Transaksi::findOrfail($id);
        $trans->noresi = $r->resi;
        $trans->save();

        return redirect()->back()->with('success','Berhasil update data');
    }

}
