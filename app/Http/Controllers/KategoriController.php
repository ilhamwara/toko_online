<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kategori;

class KategoriController extends Controller
{
    public function index()
    {
        $title = 'Category';
        $kat = Kategori::all();
        return view('admin.kategori.index',compact('title','kat'));
    }
    public function tambah()
    {
        $title = 'Add Category';
        return view('admin.kategori.tambah',compact('title'));
    }
    public function edit($id)
    {
        $title = 'Edit Category';
        $kat = Kategori::findOrFail($id);

        return view('admin.kategori.edit',compact('title','kat'));
    }
    public function post_tambah(Request $r)
    {
        $validate = $r->validate([
            'name'  => 'required',
        ]);

        if (Kategori::where('name',$r->name)->count() > 0) {
            return redirect('administrator/categories/add')->with('warning','Ooppss sorry name cannot be same');            
        }

        $kat = new Kategori;
        $kat->name = $r->name;
        $kat->save();

        return redirect('administrator/categories/add')->with('success','Success save data');
    }
    public function post_edit(Request $r,$id)
    {
        $validate = $r->validate([
            'name' => 'required',
        ]);

        if (Kategori::where('id','!=',$id)->where('name',$r->name)->count() > 0) {
            return redirect('administrator/categories/add')->with('warning','Ooppss sorry name cannot be same');            
        }

        $kat = Kategori::findOrFail($id);
        $kat->name = $r->name;
        $kat->save();


        return redirect('administrator/categories/edit/'.$id)->with('success','Success edit data');
    }
    public function delete($id)
    {
        Kategori::where('id',$id)->delete();
        return redirect('administrator/categories')->with('success','Success delete data');
    }
}
