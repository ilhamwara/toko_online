<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Hash;
use App\User;
use App\Kategori;
use App\SubKategori;
use App\Page;
use App\Menu;
use App\Setting;
use App\Banner;
use App\FotoProduk;
use App\Produk;
use App\Transaksi;
use App\Cart;
use App\CartProduk;
use Auth;
use App\Library\RajaOngkirLib;

class FrontController extends Controller
{
    public function index()
    {
        $menu_info    = Menu::where('title','Information')->get();
        $menu_insider = Menu::where('title','Insider')->get();
        $menu_service = Menu::where('title','Service')->get();
        $kat          = Kategori::all();
        $slider       = Banner::where('type','Slider')->where('id_kategori',NULL)->orderBy('urutan','ASC')->get();
        $set          = Setting::findOrFail(1);

        return view('front.index',compact('menu_info','menu_insider','menu_service','kat','set','slider'));
    }
    public function featured()
    {
        $menu_info    = Menu::where('title','Information')->get();
        $menu_insider = Menu::where('title','Insider')->get();
        $menu_service = Menu::where('title','Service')->get();
        $set          = Setting::findOrFail(1);
        $kat          = Kategori::all();
        $pro          = Produk::where('featured',1)->paginate(9);
        $pro2          = Produk::where('featured',1)->paginate(8);
        $title        = 'Featured';
        $slider       = Banner::where('type','Slider')->where('id_kategori',NULL)->orderBy('urutan','ASC')->get();
        $banner = Banner::where('type','Banner Category')->orderBy('urutan','ASC')->get();

        return view('front.produk',compact('menu_info','menu_insider','menu_service','set','kat','pro','title','banner','pro2','slider'));
    }
    public function promo()
    {
        $menu_info    = Menu::where('title','Information')->get();
        $menu_insider = Menu::where('title','Insider')->get();
        $menu_service = Menu::where('title','Service')->get();
        $set          = Setting::findOrFail(1);
        $kat          = Kategori::all();
        $pro          = Produk::where('promo',1)->paginate(9);
        $pro2          = Produk::where('promo',1)->paginate(8);
        $title        = 'Promo';

        $banner = Banner::where('type','Banner Category')->orderBy('urutan','ASC')->get();
        $slider       = Banner::where('type','Slider')->where('id_kategori',NULL)->orderBy('urutan','ASC')->get();

        return view('front.produk',compact('menu_info','menu_insider','menu_service','set','kat','pro','title','banner','pro2','slider'));
    }
    public function flash_sale()
    {
        $menu_info    = Menu::where('title','Information')->get();
        $menu_insider = Menu::where('title','Insider')->get();
        $menu_service = Menu::where('title','Service')->get();
        $set          = Setting::findOrFail(1);
        $kat          = Kategori::all();
        $pro          = Produk::where('flash_sale',1)->paginate(9);
        $pro2          = Produk::where('flash_sale',1)->paginate(8);
        $title        = 'Flash Sale';

        $slider       = Banner::where('type','Slider')->where('id_kategori',NULL)->orderBy('urutan','ASC')->get();
        $banner = Banner::where('type','Banner Category')->orderBy('urutan','ASC')->get();

        return view('front.produk',compact('menu_info','menu_insider','menu_service','set','kat','pro','title','banner','pro2','slider'));
    }
    public function new_arrival()
    {
        $menu_info    = Menu::where('title','Information')->get();
        $menu_insider = Menu::where('title','Insider')->get();
        $menu_service = Menu::where('title','Service')->get();
        $set          = Setting::findOrFail(1);
        $kat          = Kategori::all();
        $pro          = Produk::where('new_arrival',1)->paginate(9);
        $pro2         = Produk::where('new_arrival',1)->paginate(8);
        $title        = 'New Arrival';

        $slider       = Banner::where('type','Slider')->where('id_kategori',NULL)->orderBy('urutan','ASC')->get();
        $banner = Banner::where('type','Banner Category')->orderBy('urutan','ASC')->get();

        return view('front.produk',compact('menu_info','menu_insider','menu_service','set','kat','pro','title','banner','pro2','slider'));
    }
    public function kategori($slug)
    {
        $menu_info    = Menu::where('title','Information')->get();
        $menu_insider = Menu::where('title','Insider')->get();
        $menu_service = Menu::where('title','Service')->get();
        $set          = Setting::findOrFail(1);
        $kat          = Kategori::all();
        $pro          = Produk::where('id_kategori',Kategori::where('slug',$slug)->value('id'))->paginate(9);
        $pro2         = Produk::where('id_kategori',Kategori::where('slug',$slug)->value('id'))->paginate(8);
        $title        = Kategori::where('slug',$slug)->value('name');

        $slider        = Banner::where('type','Slider')->where('id_kategori',Kategori::where('slug',$slug)->value('id'))->orderBy('urutan','ASC')->get();
        $banner       = Banner::where('type','Banner Category')->orderBy('urutan','ASC')->get();

        return view('front.produk',compact('menu_info','menu_insider','menu_service','set','kat','pro','title','banner','pro2','slider'));
    }
    public function subkategori($katnya,$sub)
    {
        $menu_info    = Menu::where('title','Information')->get();
        $menu_insider = Menu::where('title','Insider')->get();
        $menu_service = Menu::where('title','Service')->get();
        $set          = Setting::findOrFail(1);
        $kat          = Kategori::all();
        $subnya       = SubKategori::where('id_kategori',Kategori::where('slug',$katnya)->value('id'))->where('slug',$sub)->first();
        $pro          = Produk::where('id_kategori',Kategori::where('slug',$katnya)->value('id'))->where('id_subkategori',$subnya->id)->paginate(9);
        $pro2          = Produk::where('id_kategori',Kategori::where('slug',$katnya)->value('id'))->where('id_subkategori',$subnya->id)->paginate(8);
        $title        = SubKategori::where('slug',$sub)->value('name');

        $slider        = Banner::where('type','Slider')->where('id_kategori',Kategori::where('slug',$katnya)->value('id'))->orderBy('urutan','ASC')->get();
        $banner       = Banner::where('type','Banner Category')->orderBy('urutan','ASC')->get();

        return view('front.produk',compact('menu_info','menu_insider','menu_service','set','kat','pro','title','banner','subnya','pro2','katnya','slider'));
    }
    public function produk()
    {
        $menu_info    = Menu::where('title','Information')->get();
        $menu_insider = Menu::where('title','Insider')->get();
        $menu_service = Menu::where('title','Service')->get();
        $set          = Setting::findOrFail(1);
        $kat          = Kategori::all();
        $pro          = Produk::where('featured',0)->where('flash_sale',0)->where('new_arrival',0)->where('promo',0)->paginate(9);
        $pro2          = Produk::where('featured',0)->where('flash_sale',0)->where('new_arrival',0)->where('promo',0)->paginate(8);
        $title        = 'Products';


        $banner = Banner::where('type','Banner Category')->orderBy('urutan','ASC')->get();

        return view('front.produk',compact('menu_info','menu_insider','menu_service','set','kat','pro','title','banner','pro2'));
    }
    public function produkdetail($slug)
    {
        $menu_info    = Menu::where('title','Information')->get();
        $menu_insider = Menu::where('title','Insider')->get();
        $menu_service = Menu::where('title','Service')->get();
        $set          = Setting::findOrFail(1);
        $kat          = Kategori::all();
        $pro          = Produk::where('slug',$slug)->first();
        $subnya       = SubKategori::where('id',$pro->id_subkategori)->first();
        $foto = FotoProduk::where('id_produk',$pro->id)->get();
        if (!empty($subnya)) {
            $related = Produk::where('id','!=',$pro->id)->where('id_kategori',$pro->id_kategori)->inRandomOrder()->limit(5)->get();
        }else{
            $related = Produk::where('id','!=',$pro->id)->where('id_kategori',$pro->id_kategori)->where('id_subkategori',$pro->id_subkategori)->inRandomOrder()->limit(5)->get();
        }

        return view('front.produk-detail',compact('menu_info','menu_insider','menu_service','kat','set','pro','subnya','related','foto'));
    }
    public function post_cart(Request $r)
    {
        $validate = $r->validate([
            'qty'      => 'required|integer',
            'produk'   => 'required|integer',
        ]);
        if (!Auth::check()) {
            return redirect('login')->with('warning','You must login to your account first');
        }
        if (Cart::where('id_user',Auth::user()->id)->where('id_produk',$r->produk)->count() > 0) {
            Cart::where('id_user',Auth::user()->id)->where('id_produk',$r->produk)->delete();
        }

        $pro = Produk::findOrFail($r->produk);
        $cart = new Cart;
        $cart->id_user      = Auth::user()->id;
        $cart->id_produk    = $r->produk;
        $cart->qty          = $r->qty;
        $cart->total        = ($pro->diskon > 0 ? ($pro->harga - (($pro->diskon/100)*$pro->harga))*$r->qty : ($pro->harga*$r->qty));
        $cart->save();

        return redirect('cart');
    }
    public function hapus_cart($id)
    {
        Cart::findOrFail($id)->delete();
        return redirect()->back()->with('success','Success Delete Cart');
    }
    public function page($page)
    {
        $menu_info    = Menu::where('title','Information')->get();
        $menu_insider = Menu::where('title','Insider')->get();
        $menu_service = Menu::where('title','Service')->get();
        $kat          = Kategori::all();
        $set = Setting::findOrFail(1);


        $page_menu = Menu::where('link',$page)->first();
        $page = Page::where('menu',$page_menu->id)->first();
        return view('front.page',compact('page','menu_info','menu_insider','menu_service','kat','set'));
    }
    public function login()
    {
        if (Auth::check()) {
            return redirect()->back();
        }
        $menu_info = Menu::where('title','Information')->get();
        $menu_insider = Menu::where('title','Insider')->get();
        $menu_service = Menu::where('title','Service')->get();
        $set = Setting::findOrFail(1);
        $kat          = Kategori::all();

        return view('front.login',compact('menu_info','menu_insider','menu_service','set','kat'));
    }
    public function postlogin(Request $r)
    {
        if (Auth::check()) {
            return redirect()->back();
        }
        $validate = $r->validate([
            'email'     => 'required|string|email',
            'password'  => 'required',
        ]);

        if(Auth::attempt(['email' => $r->email, 'password' => $r->password,'type' => 'member'])) {
            return redirect()->intended('memberarea')->with('success','Anda berhasil login');
        }else{
            return redirect()->back()->with('error','Ooppss terdapat kesalahan login');
        }
    }
    public function register()
    {
        $menu_info = Menu::where('title','Information')->get();
        $menu_insider = Menu::where('title','Insider')->get();
        $menu_service = Menu::where('title','Service')->get();
        $kat          = Kategori::all();
        $set = Setting::findOrFail(1);

        return view('front.register',compact('menu_info','menu_insider','menu_service','set','kat'));
    }
    public function postregister(Request $r)
    {
        $validate = $r->validate([
            'name'      => 'required|string|max:255',
            'email'     => 'required|string|email|max:255|unique:users',
            'alamat'    => 'required|string|max:255',
            'kota'      => 'required|string|max:255',
            'phone'     => 'required|string|max:255',
            'password'  => 'required',
        ]);

        $user = new User;
        $user->name      = $r->name;
        $user->email     = $r->email;
        $user->alamat    = $r->alamat;
        $user->kota      = $r->kota;
        $user->phone     = $r->phone;
        $user->password  = Hash::make($r->password);
        $user->type      = 'member';
        $user->save();

        return redirect('register')->with('success','Berhasil daftar akun baru');

    }
    public function logout()
    {
        if (Auth::user()->type == 'member') {
            Auth::logout();
            return redirect('login')->with('success','Berhasil logout');
        }else{
            Auth::logout();
            return redirect('administrator/login')->with('success','Berhasil logout');
        }
    }
    public function cart()
    {
        if(!Auth::check()) {
            return redirect('login')->with('warning','Ooppss sorry you must login');
        }
        $menu_info    = Menu::where('title','Information')->get();
        $menu_insider = Menu::where('title','Insider')->get();
        $menu_service = Menu::where('title','Service')->get();
        $kat          = Kategori::all();
        $set          = Setting::findOrFail(1);
        $cart         = Cart::where('id_user',Auth::user()->id)->get();
        $provinsi     = RajaOngkirLib::provinsi();

        return view('front.cart',compact('menu_info','menu_insider','menu_service','set','kat','cart','provinsi'));
    }
    public function checkout()
    {
        $menu_info = Menu::where('title','Information')->get();
        $menu_insider = Menu::where('title','Insider')->get();
        $menu_service = Menu::where('title','Service')->get();
        $kat          = Kategori::all();
        $set = Setting::findOrFail(1);

        return view('front.checkout',compact('menu_info','menu_insider','menu_service','kat','set'));
    }
}
