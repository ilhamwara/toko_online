<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use URL;
use App\Produk;
use App\FotoProduk;
use App\Kategori;
use App\SubKategori;
use Intervention\Image\ImageManager;

class ProdukController extends Controller
{
    public function index()
    {
        if (request()->segment(3) == 'list') {
            $title = 'Product';
            $pro   = Produk::where('aktif',1)->get();
        }elseif (request()->segment(3) == 'featured') {
            $title = 'Product Featured';
            $pro   = Produk::where('featured',1)->where('aktif',1)->get();
        }elseif (request()->segment(3) == 'new_arrival') {
            $title = 'Product New Arrival';
            $pro   = Produk::where('new_arrival',1)->where('aktif',1)->get();
        }elseif (request()->segment(3) == 'promo') {
            $title = 'Product Promo';
            $pro   = Produk::where('promo',1)->where('aktif',1)->get();
        }elseif (request()->segment(3) == 'flash_sale') {
            $title = 'Product Flash Sale';
            $pro   = Produk::where('flash_sale',1)->where('aktif',1)->get();
        }

        return view('admin.produk.index',compact('title','pro'));
    }
    public function tambah()
    {
        $title = 'Add Product';
        $kat = Kategori::all();
        $sub = SubKategori::all();
        return view('admin.produk.tambah',compact('title','kat','sub'));
    }
    public function edit($id)
    {
        $title = 'Edit Product';
        $pro = Produk::findOrFail($id);
        $kat = Kategori::all();
        $sub = SubKategori::all();

        return view('admin.produk.edit',compact('title','pro','kat','sub'));
    }
    public function post_tambah(Request $r)
    {
        $validate = $r->validate([
            'name'            => 'required|string',
            'id_kategori'     => 'required|integer',
            'description'     => 'required|string',
            'tag'             => 'required|string',
            'diskon'          => 'required|integer',
            'harga'           => 'required|integer',
            'stock'           => 'required|integer',
        ]);

        if (Produk::where('name',$r->name)->count() > 0) {
            return redirect()->back()->with('warning','Maaf nama tidak boleh sama');
        }

        $pro = new Produk;
        $pro->id_kategori    = $r->id_kategori;
        $pro->id_subkategori = $r->id_subkategori;
        $pro->slug           = str_slug($r->name);
        $pro->name           = $r->name;
        $pro->description    = $r->description;
        $pro->tag            = $r->tag;
        $pro->diskon         = $r->diskon;
        $pro->harga          = $r->harga;
        $pro->stock          = $r->stock;

        if ($r->type == 'Featured') {
            $pro->featured   = 1;
        }elseif ($r->type == 'New Arrival') {
            $pro->new_arrival= 1;    
        }elseif ($r->type == 'Promo') {
            $pro->promo      = 1;
        }elseif ($r->type == 'Flash Sale') {
            $pro->flash_sale = 1;
        }else{

        }

        $pro->aktif          = 1;
        $pro->save();


        if (!empty($r->img)) {
            $cek_foto = FotoProduk::orderBy('id','DESC')->first();
            foreach ($r->img as $k => $v) {
                if(($v->getClientOriginalExtension() == 'svg') || ($v->getClientOriginalExtension() == 'jpg') || ($v->getClientOriginalExtension() == 'jpeg') || ($v->getClientOriginalExtension() == 'png') ){
                    $img_full[$k]         = str_slug($r->name).'_img_full_'.(empty($cek_foto) ? 0 : $cek_foto->id).'.'.$v->getClientOriginalExtension();
                    $img_thumbnail1[$k]   = str_slug($r->name).'_img_thumbnail1_'.(empty($cek_foto) ? 0 : $cek_foto->id).'.'.$v->getClientOriginalExtension();
                    $img_thumbnail2[$k]   = str_slug($r->name).'_img_thumbnail2_'.(empty($cek_foto) ? 0 : $cek_foto->id).'.'.$v->getClientOriginalExtension();

                    $manager[$k]      = new ImageManager();
                    $manager[$k]->make($v)->save(public_path().'/images/products/'.$img_full[$k]);
                    $manager[$k]->make($v)->resize(172,172)->save(public_path().'/images/products/'.$img_thumbnail1[$k]);
                    $manager[$k]->make($v)->resize(91,91)->save(public_path().'/images/products/'.$img_thumbnail2[$k]);

                    $foto[$k]                   = new FotoProduk;
                    $foto[$k]->id_produk        = $pro->id;
                    $foto[$k]->img_full         = $img_full[$k];
                    $foto[$k]->img_thumbnail1   = $img_thumbnail1[$k];
                    $foto[$k]->img_thumbnail2   = $img_thumbnail2[$k];
                    $foto[$k]->save();

                }else{
                    return redirect()->back()->with('error', 'Format yang anda masukan bukan gambar');
                }
            }
        }   

        return redirect('administrator/products/add')->with('success','Success save data');
    }
    public function post_edit(Request $r,$id)
    {
        $validate = $r->validate([
            'name'            => 'required|string',
            'id_kategori'     => 'required|integer',
            'description'     => 'required|string',
            'tag'             => 'required|string',
            'diskon'          => 'required|integer',
            'harga'           => 'required|integer',
            'stock'           => 'required|integer',
        ]);

        if (Produk::where('id','!=',$id)->where('name',$r->name)->count() > 0) {
            return redirect()->back()->with('warning','Maaf nama tidak boleh sama');
        }

        $pro = Produk::findOrFail($id);
        $pro->id_kategori    = $r->id_kategori;
        $pro->id_subkategori = $r->id_subkategori;
        $pro->slug           = str_slug($r->name);
        $pro->name           = $r->name;
        $pro->description    = $r->description;
        $pro->tag            = $r->tag;
        $pro->diskon         = $r->diskon;
        $pro->harga          = $r->harga;
        $pro->stock          = $r->stock;

        if ($r->type == 'Featured') {
            $pro->featured   = 1;
        }elseif ($r->type == 'New Arrival') {
            $pro->new_arrival= 1;    
        }elseif ($r->type == 'Promo') {
            $pro->promo      = 1;
        }elseif ($r->type == 'Flash Sale') {
            $pro->flash_sale = 1;
        }

        $pro->aktif          = 1;
        $pro->save();


        if (!empty($r->img)) {
            $cek_foto = FotoProduk::orderBy('id','DESC')->first();
            foreach ($r->img as $k => $v) {
                if(($v->getClientOriginalExtension() == 'svg') || ($v->getClientOriginalExtension() == 'jpg') || ($v->getClientOriginalExtension() == 'jpeg') || ($v->getClientOriginalExtension() == 'png') ){
                    $img_full[$k]         = str_slug($r->name).'_img_full_'.(empty($cek_foto) ? 0 : $cek_foto->id).'.'.$v->getClientOriginalExtension();
                    $img_thumbnail1[$k]   = str_slug($r->name).'_img_thumbnail1_'.(empty($cek_foto) ? 0 : $cek_foto->id).'.'.$v->getClientOriginalExtension();
                    $img_thumbnail2[$k]   = str_slug($r->name).'_img_thumbnail2_'.(empty($cek_foto) ? 0 : $cek_foto->id).'.'.$v->getClientOriginalExtension();

                    $manager[$k]      = new ImageManager();
                    $manager[$k]->make($v)->save(public_path().'/images/products/'.$img_full[$k]);
                    $manager[$k]->make($v)->resize(172,172)->save(public_path().'/images/products/'.$img_thumbnail1[$k]);
                    $manager[$k]->make($v)->resize(91,91)->save(public_path().'/images/products/'.$img_thumbnail2[$k]);

                    $foto[$k]                   = new FotoProduk;
                    $foto[$k]->id_produk        = $pro->id;
                    $foto[$k]->img_full         = $img_full[$k];
                    $foto[$k]->img_thumbnail1   = $img_thumbnail1[$k];
                    $foto[$k]->img_thumbnail2   = $img_thumbnail2[$k];
                    $foto[$k]->save();

                }else{
                    return redirect()->back()->with('error', 'Format yang anda masukan bukan gambar');
                }
            }
        }

        return redirect('administrator/products/edit/'.$id)->with('success','Success edit data');
    }
    public function delete($id)
    {
        Produk::where('id',$id)->update(['aktif' => 0]);
        return redirect('administrator/products')->with('success','Success delete data');
    }
}
